# concrete-db

a read-only web database generator

## License

[MIT](LICENSE)

## Manual

The official manual is maintained at [https://cepharum-foss.gitlab.io/concrete-db](https://cepharum-foss.gitlab.io/concrete-db).
