# Terms

[Shapes](../glossary.md#shape) commonly rely on [terms](../glossary.md#term) to compute values in several _contexts_. They are supported by a [simple but flexible terms processor](https://cepharum-foss.gitlab.io/simple-terms/) which is enforcing a [certain syntax](https://cepharum-foss.gitlab.io/simple-terms/language.html). Terms always compute a single value based on data available per context. 

## Functions

Terms are capable of invoking case-insensitively named functions. Available functions are compiled from different sources.

### Generic functions

There are a lot of [commonly useful functions](https://cepharum-foss.gitlab.io/simple-terms/functions.html) provided by the term processor itself.

:::warning No browser support!
There are functions working in context of a browser, only, such as [`cookie()`](https://cepharum-foss.gitlab.io/simple-terms/functions.html#cookie). Databases aren't cured in context of a browser, thus those function don't work as described.
:::

### Database functions

Additional term functions are provided by ConcreteDB. Refer to the [related reference manual](/api/functions.md) for details.

### Custom functions

On every curing of a database, additional function libraries can be provided to export custom functions for use in terms. For example, if there is a file named **mylib/functions.mjs** containing

```javascript
export function volume( a, b, c ) {
  return a * b * c;
}
```

you can include that file as a library on curing a database with a command like

```bash
cure -o output -l mylib/functions.mjs source1 source2
```

so that your shape can use the exported function `volume()` in terms like this:

```javascript
size: VOLUME( width, height, depth )
```

:::warning Some constraints apply!
* Exported functions' names may consist of lowercase letters, only.
* They have to return a result instantly. Promises are not yet supported.
* By intention, no custom function is allowed to replace functions of terms processor or ConcreteDB. 
:::

## Contexts

Terms are processed in different _contexts_ with computable data originating from certain sources - the context's _data space_ - and particular naming conventions applying in either context.

### Compiling items

On [compiling an item from a record](/about/curing.md#compiling-model-items) all [its properties](../api/shapes.md#properties) are defined as terms to be computed. This includes special properties for detecting [model](../api/shapes.md#model) and [ID](../api/shapes.md#id) of item to compile.

#### Data space

Item properties are computed per record. Either record is used as the terms' data space with case-insensitive names. 

In addition, some [meta properties](../api/shapes.md#meta-properties) are successively provided:

* [`$segments`](/api/shapes.md#segments) is exposed on every term evaluated here.

* [`$model`](/api/shapes.md#model) is exposed after computing [rule](../api/shapes.md#model) of [common](/api/shapes.md#common) [model shape](/api/shapes.md#model-shape) for discovering an item's model.

* [`$shape`](/api/shapes.md#shape) is exposing the item's [model shape](shapes.md#model-shape) after discovering its model.

* [`$id`](/api/shapes.md#id) is exposing the item's unique ID as a result of [computing related rule](../api/shapes.md#id) of its [model shape](shapes.md#model-shape).

#### Defaults

A [separate set of rules per shape](../api/shapes.md#defaults) is declaring default values. 

Remember, any [item of a model may be compiled from multiple records](curing.md#compiling-model-items). On first record, defaults are considered for every property that starts with a nullish value after computing its term in context of that record. Defaults are ignored when processing additional records for the same item. 

Default declarations are literal values in the majority of cases for they should work independently of a particular record serving as a data source. But terms are supported nonetheless. However, terms have to start with an assignment operator here (like in common spreadsheet applications).

:::tip Example
```yaml
defaults:
  priority: random( 1, 100 )
```

This example assigns literal string `"random( 1, 100 )"` to a freshly compiled item's property named `prio` if computing that property from first related record results in a nullish value.

```yaml
defaults:
  label: =random( 1, 100 )
```

This slightly different example is assigning the result of computing term `random( 1, 100 )` instead, which is delivering a random number between 1 and 100.
:::

Terms declaring computed defaults are processed in context of same data space as terms for computing properties from a record before.

#### Case-insensitive data

All properties of a collected record are exposed to a processed term in a case-insensitive way. If a term is addressing a property like `foo` or `FOO`, it is actually capable of reading property `FOO`, `fOO`, `Foo` or `fOo` of collected record. 

Mixed case properties in source files are supported to improve convenience and fault tolerance on processing records probably managed by non-technical users.

:::warning Note
Additionally provided meta properties are case-sensitive.
:::

:::warning Mixed Case Ambiguities
A record might contain multiple occurrences of a case-insensitively named property:

```yaml
foo: bar
FoO: BAR
```

In this case derivation fails to pick the right property to read.
:::

### Contributing to items

Every compiled item can be declared to [contribute to properties of one or more other items](curing.md#processing-contributions). For every such property, a term is given to compute the contributed value of either item based on its own properties and its record.

```yaml
models:
  book:
    ...
    contributions:
      author:
        $id: asArray( author )
        books[]: $id
```

This basically works the same way as before when [compiling the contributing item from a record](#compiling-items), though a few differences apply:

* The data space consists of the item's properties as computed and collected from one or more records so far. The current record is still available through meta property [`$original`](../api/shapes.md#original).

* The exposed shape of item in [`$shape`](../api/shapes.md#shape) is an attempt to merge all local shapes of records that have been processed with regard to the contributing item so far.

### Recompiling with contributions

Every item which has [received contributions](curing.md#processing-contributions) by other items is re-compiled eventually. This basically works the same way as before when [compiling it from a record](#compiling-items). But a few differences apply:

* There is no record serving as the data space for processing terms to compute item's properties. Instead, the existing item including its contributions is used.

* Accordingly, there is no local shape in particular, but a shape merged from local shapes of all records processed before.

* An additional meta property [`$post`](../api/shapes.md#post) is set so that properties' terms can compute differently.

  ```yaml
  properties:
    relatedItems: first( relatedItems, array() )
    numRelatedItems: test( $post, length( relatedItems ), 0 )
  ```
  
  This example is assigning `0` to property `numRelatedItems` on compiling item from record and counting actual number of contributed relations on re-compiling.

  :::tip Note!
  Often, items with contributions happen to exist only because of those contributions. Thus, differing between compiling and re-compiling isn't required for they get re-compiled, only.
  :::

### On generating collections

When [creating collections as part of shaping process](/about/curing.md#creating-collections), rules of shape defining properties to expose per item in either collection are assumed to contain terms, too.

#### Data space

In opposition to the [stage of compiling items](#compiling-items), terms involved in [creating a collection](/api/collections.md) are computed in a context exposing properties of either item processed before to become part of a collection. Those items have case-sensitive properties due to that compilation, thus they are exposed [case-sensitively](#case-sensitive-data) here, too.

This context is augmented by a similar set of meta properties:

* [`$collection`](../api/shapes.md#collection) is exposing the definition of the collection current item is processed for.

  ```yaml
  properties:
    version: $collection.version
  ```

* [`$database`](../api/shapes.md#models) exposes all models' items in a two-level hierarchy mapping model names into maps of item IDs into either item's data.

  ```yaml
  filter: price < $database.friend.john-doe.spentMoney / 10
  ```

* [`$model`](/api/shapes.md#model) names the item's own model.

* [`$id`](/api/shapes.md#id) exposes either item's ID.

#### Case-sensitive data

Data exposed in this context is due to deriving items from a record which in turn depends on a shape written by a software developer or a similarly skilled person. To relax the implementation of curing process, reading data in this context works case-sensitively. Either property's name is due to its definition in the shape.

### On sorting regular collections

When [sorting](/api/collections.md#sort) [entries of a collection](/api/collections.md#entries-of-collection-vs-items-of-model) with a [term](/about/terms.md), it is computed in yet another context.

#### Data space

In term-based sorting, the provided term is evaluated repeatedly to compare two of the collection's entries. They are exposed as `$left` and `$right` available for accessing either entry's properties. 

The term is expected to result in

* value less than 0 if `$left` < `$right`,
* value greater than 0 if `$left` > `$right` or
* 0 if they are equivalent.

```yaml
sort: icompare( $left.label, $right.label )
```

In addition, either entry's originating item of model is exposed using sub-property `$source`.

```yaml
sort: icompare( $left.$source.firstname, $right.$source.firstname )
```

This enables term-based sorting to process item properties without exposing them in related entry of collection.

#### Case-sensitive data

Data exposed in this context is due to deriving items from a record which in turn depends on a shape written by a software developer or a similarly skilled person. To relax the implementation of curing process, reading data in this context works case-sensitively. Either property's name is due to its definition in the shape.
