import { describe, it } from "mocha";
import "should";
import merge from "lodash.merge";

import { cure } from "../../lib/index.mjs";
import { FakeCollector, FakeGenerator, fakeModelShape, qualifyFakedRecords } from "./helper.mjs";


describe( "Curing: creating collection with sorted list of items", () => {
	it( "generates regular collection endpoints for all items sorted by named property", async() => {
		const generator = new FakeGenerator();
		const source = data();
		const collector = new FakeCollector( source, source.$shape );

		await cure( [], {}, { collector, generator } );

		const sortedFriends = generator.writtenDatabase.endpoints["api/v1/friend"];
		sortedFriends.count.should.be.Number().which.is.equal( 6 );
		sortedFriends.items.map( i => i.surname.toLowerCase() ).should.be.deepEqual( [
			"smith", "parker", "doe", "doe", "doe", "alistair",
		] );

		const sortedAdversaries = generator.writtenDatabase.endpoints["api/v1/adversary"];
		sortedAdversaries.count.should.be.Number().which.is.equal( 7 );
		sortedAdversaries.items.map( i => i.surname.toLowerCase() ).should.be.deepEqual( [
			"grape", "lewis", "lewis", "lewis", "miller", "miller", "zorg",
		] );
	} );

	it( "generates regular collection endpoints for all items sorted by term", async() => {
		const generator = new FakeGenerator();
		const source = data();
		const collector = new FakeCollector( source, source.$shape );

		await cure( [], {}, { collector, generator } );

		const sortedFriends = generator.writtenDatabase.endpoints["api/v1/friend/term"];
		sortedFriends.count.should.be.Number().which.is.equal( 6 );
		sortedFriends.items.map( i => ( i.surname + ", " + i.firstname ).toLowerCase() ).should.be.deepEqual( [
			"alistair, mika", "doe, jane", "doe, john", "doe, karl", "parker, peter", "smith, maxine",
		] );

		const sortedAdversaries = generator.writtenDatabase.endpoints["api/v1/adversary/term"];
		sortedAdversaries.count.should.be.Number().which.is.equal( 7 );
		sortedAdversaries.items.map( i => ( i.surname + ", " + i.firstname ).toLowerCase() ).should.be.deepEqual( [
			"miller, tom", "miller, paula", "zorg, jean-baptiste emanuel", "lewis, jason", "grape, gilbert", "lewis, anthony", "lewis, agatha",
		] );
	} );

	it( "rejects generation of keyed collection sorted by property which isn't exposed in collection", async() => {
		const generator = new FakeGenerator();
		const source = data( {
			common: {
				collections: {
					keyed: {
						sort: {
							property: "surname",
						},
						reduce: true,
					}
				}
			}
		} );
		const collector = new FakeCollector( source, source.$shape );

		await cure( [], {}, { collector, generator } ).should.be.rejectedWith( /\Wsurname\W/ );
	} );

	it( "generates keyed collection endpoints for all items per key sorted by named property", async() => {
		const generator = new FakeGenerator();
		const source = data();
		const collector = new FakeCollector( source, source.$shape );

		await cure( [], {}, { collector, generator } );

		const sortedFriends = generator.writtenDatabase.endpoints["api/v1/friend/keyed"];
		sortedFriends.should.be.deepEqual( {
			DOE: {
				count: 3,
				items: [ {
					$id: "karl",
					label: "Doe, karl",
					firstname: "karl",
				}, {
					$id: "john",
					label: "Doe, John",
					firstname: "John",
				}, {
					$id: "jane",
					label: "doe, Jane",
					firstname: "Jane",
				} ],
			},
			ALISTAIR: {
				count: 1,
				items: [{
					$id: "mika",
					label: "Alistair, Mika",
					firstname: "Mika",
				}],
			},
			SMITH: {
				count: 1,
				items: [{
					$id: "max",
					label: "Smith, Maxine",
					firstname: "Maxine",
				}],
			},
			PARKER: {
				count: 1,
				items: [{
					$id: "peter",
					label: "Parker, Peter",
					firstname: "Peter",
				}],
			},
		} );

		const sortedAdversaries = generator.writtenDatabase.endpoints["api/v1/adversary/keyed"];
		sortedAdversaries.should.be.deepEqual( {
			MILLER: {
				count: 2,
				items: [ {
					$id: "paula",
					label: "Miller, Paula",
					firstname: "Paula",
				}, {
					$id: "tom",
					label: "Miller, Tom",
					firstname: "Tom",
				} ],
			},
			LEWIS: {
				count: 3,
				items: [ {
					$id: "agatha",
					label: "lewis, agatha",
					firstname: "agatha",
				}, {
					$id: "anthony",
					label: "Lewis, Anthony",
					firstname: "Anthony",
				}, {
					$id: "jason",
					label: "Lewis, Jason",
					firstname: "Jason",
				} ],
			},
			GRAPE: {
				count: 1,
				items: [{
					$id: "gilbert",
					label: "Grape, Gilbert",
					firstname: "Gilbert",
				}],
			},
			ZORG: {
				count: 1,
				items: [{
					$id: "zorg",
					label: "Zorg, Jean-Baptiste Emanuel",
					firstname: "Jean-Baptiste Emanuel",
				}],
			},
		} );
	} );

	it( "generates keyed collection endpoints for all items per key sorted by term", async() => {
		const generator = new FakeGenerator();
		const source = data();
		const collector = new FakeCollector( source, source.$shape );

		await cure( [], {}, { collector, generator } );

		const sortedFriends = generator.writtenDatabase.endpoints["api/v1/friend/keyed-term"];
		sortedFriends.should.be.deepEqual( {
			doe: {
				count: 3,
				items: [ {
					$id: "karl",
					label: "Doe, karl",
				}, {
					$id: "john",
					label: "Doe, John",
				}, {
					$id: "jane",
					label: "doe, Jane",
				} ],
			},
			alistair: {
				count: 1,
				items: [{
					$id: "mika",
					label: "Alistair, Mika",
				}],
			},
			smith: {
				count: 1,
				items: [{
					$id: "max",
					label: "Smith, Maxine",
				}],
			},
			parker: {
				count: 1,
				items: [{
					$id: "peter",
					label: "Parker, Peter",
				}],
			},
		} );

		const sortedAdversaries = generator.writtenDatabase.endpoints["api/v1/adversary/keyed-term"];
		sortedAdversaries.should.be.deepEqual( {
			miller: {
				count: 2,
				items: [ {
					$id: "paula",
					label: "Miller, Paula",
				}, {
					$id: "tom",
					label: "Miller, Tom",
				} ],
			},
			lewis: {
				count: 3,
				items: [ {
					$id: "agatha",
					label: "lewis, agatha",
				}, {
					$id: "anthony",
					label: "Lewis, Anthony",
				}, {
					$id: "jason",
					label: "Lewis, Jason",
				} ],
			},
			grape: {
				count: 1,
				items: [{
					$id: "gilbert",
					label: "Grape, Gilbert",
				}],
			},
			zorg: {
				count: 1,
				items: [{
					$id: "zorg",
					label: "Zorg, Jean-Baptiste Emanuel",
				}],
			},
		} );
	} );
} );


function data( shapeOverlay ) {
	const localShape = shape();

	if ( shapeOverlay ) {
		merge( localShape, shapeOverlay );
	}

	return qualifyFakedRecords( {
		"data/jane": { model: "friend", id: "jane", surname: "doe", firstname: "Jane" },
		"data/mika": { model: "friend", id: "mika", surname: "Alistair", firstname: "Mika" },
		"data/john": { model: "friend", id: "john", surname: "Doe", firstname: "John" },
		"data/peter": { model: "friend", id: "peter", surname: "Parker", firstname: "Peter" },
		"data/karl": { model: "friend", id: "karl", surname: "Doe", firstname: "karl" },
		"data/max": { model: "friend", id: "max", surname: "Smith", firstname: "Maxine" },

		"data/paula": { model: "adversary", id: "paula", surname: "Miller", firstname: "Paula" },
		"data/jason": { model: "adversary", id: "jason", surname: "Lewis", firstname: "Jason" },
		"data/gilbert": { model: "adversary", id: "gilbert", surname: "Grape", firstname: "Gilbert" },
		"data/tom": { model: "adversary", id: "tom", surname: "Miller", firstname: "Tom" },
		"data/agatha": { model: "adversary", id: "agatha", surname: "lewis", firstname: "agatha" },
		"data/anthony": { model: "adversary", id: "anthony", surname: "Lewis", firstname: "Anthony" },
		"data/jp": { model: "adversary", id: "zorg", surname: "Zorg", firstname: "Jean-Baptiste Emanuel" },
	}, localShape );
}

function shape() {
	return fakeModelShape( "friend", {
		collections: {
			"": {
				sort: { descending: true },
			},
			term: {
				sort: "icompare( $left.surname, $right.surname ) || icompare( $left.firstname, $right.firstname )",
			},
			keyed: {
				sort: { descending: true },
				reduce: true,
			},
			"keyed-term": {
				sort: "icompare( $right.$source.firstname, $left.$source.firstname )",
				reduce: true,
			},
		},
	}, {
		properties: {
			$model: "model || item( $segments, length( $segments ) - 2 )",
			$id: "id || item( $segments, length( $segments ) - 1 )",
			surname: "surname",
			firstname: "firstname",
		},
		collections: {
			"": {
				sort: { property: "surname" },
			},
			term: {
				sort: "icompare( $right.firstname, $left.firstname )",
			},
			keyed: {
				key: "uppercase( surname )",
				sort: { property: "firstname" },
				properties: {
					label: "surname + ', ' + firstname",
					firstname: "firstname",
				},
				reduce: true,
			},
			"keyed-term": {
				key: "lowercase( surname )",
				sort: "icompare( $left.$source.firstname, $right.$source.firstname )",
				properties: {
					label: "surname + ', ' + firstname",
				},
				reduce: true,
			},
		},
	} );
}
