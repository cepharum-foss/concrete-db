# Shapes

Data processing is always based on defined structures to support. There are many tools and techniques available for establishing and maintaining a valid structure for any data. _Schema_ has been coined as a term for sets of rules expressing conditions for a data structure to be considered _valid_. You'll find schemata in context of database engines like SQLite and formats like XML, JSON and YAML.

ConcreteDB relies on structural definition of data to be processed, too. One might think: Due to converting YAML into JSON, ConcreteDB could simply go with their common concept of a schema. Supporting JSON schema for assuring validity of data is an option for the future. But a schema declares what is valid in a particular record. 

```mermaid
graph LR
	D[data]
	S[schema]
	P([validator])
	R{{"data is valid!"}}
	D-->P
	S-->P
	P-.->R
```

It does not cover additional aspects of ConcreteDB such as the views to create for a model and the transformations to use for changing YAML structures into JSON structures. That's what _shapes_ are meant for.

```mermaid
graph LR
	D[data]
	S[shape]
	P([curing])
	R[different data]
	D-->P
	S-->P
	P-->R
```

Shapes control the curing process of a ConcreteDB. By doing so, they enforce a certain structure of resulting data and how to derive it from whatever data is presented as source. For the resulting data is read-only there is no need to maintain data validity after curing process has finished and so it is okay to go without with a schema ... for now.

## Sources of shapes

### Global shape

The curing process accepts provision of a global shape as part of its options. The CLI script accepts option `-s` followed by the name of a YAML file containing global shape to use.

When omitted, [defaults](https://gitlab.com/cepharum-foss/concrete-db/-/blob/master/lib/default.shape.yaml) are applied.

### Local shapes

When collecting record files from one or more base folders, a _local shape_ is accepted to overlay or replace the global shape. This local shape is read from a file named `.concrete-db.shape.yaml`. It must reside in either base folder.

A local shape is merged with the global shape unless its property [`root`](/api/shapes.md#root) is set to use the local shape instead of the global one.

Local shapes apply to all records found in that base folder.

## Model-related information per shape

Any global or local shape includes a subset of rules controlling generation of [items](/glossary.md#item) and their [collections](/glossary.md#collection) grouped by [models](/glossary.md#model). In this manual, the terms [_common shape_](/api/shapes.md#common) and [_model shape_](/api/shapes.md#model-shape) are referring to those subsets of rules.

### Model shape

For every model there can be a part of local shape describing how to derive its items and collections for the resulting database. This term is referring to the particular structure of such a rule set as well as to any such set of rules related to a particular model.

### Common shape

The common shape is basically another model shape which is used by default to process items and collections of models that lack an individual model shape.

It plays a crucial part in detecting a record's model which is a prerequisite for selecting any model shape.

## Shape syntax

Shape syntax is [described in a separate document](/api/shapes.md).
