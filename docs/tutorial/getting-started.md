# Getting Started

This brief tutorial is demonstrating the way ConcreteDB is working from a user's perspective.

## Prerequisites

ConcreteDB is written in Javascript. It is designed to run on a server. It doesn't support browsers. That's why [Node.js](https://nodejs.org/en/download/) is required for running ConcreteDB.

## Installation

For a quick start, you can open command line prompt and run

```bash
npm install -g @cepharum/concrete-db
```

to _globally_ install ConcreteDB on your computer making it available to multiple projects at once. 

:::tip Advanced setup
In a more sophisticated context, you should create a project first and install ConcreteDB as a local development dependency of that project.

```bash
mkdir my-project
cd my-project
npm init -y
npm install -D @cepharum/concrete-db
```
:::

## Setup data pool

Create a new folder meant to contain source files of your database:

```bash
mkdir my-data
```

A local [shape](/about/shapes.md#local-shapes) file **my-data/.concrete-db.shape.yaml** with the following content is describing the resulting database and its REST-like API: 

```yaml
models:
  poi:
    properties:
      lat: lat || latitude
      lng: lng || long || longitude
      label: label || title
      description: description || text
      tags: slugify( asarray( type || tags ) )
```

This example is declaring a model named `poi` to have properties `lat`, `lng`, `label`, `description` and `tags` per item. Either instance is compiled based on this declaration and its properties are computed from record files using [simple terms](https://cepharum-foss.gitlab.io/simple-terms/) written next to each property name in that shape.

Due to some internal defaults of ConcreteDB, a basic collection listing all available items is provided, too. 

## Generate data

Next, create your first [record](/glossary.md#record) file **my-data/bg.record.yaml** describing a point of interest:

```yaml
MODEL: poi
id: brandenburg-gate
Lat: 52.51628
Long: 13.37771
Label: Brandenburger Tor
type:
  - monument
  - Landmark
  - building
  - public
```

Create another record file **my-data/tvtower.record.yaml** with a similar description of another point of interest:

```yaml
Model: poi
ID: berlin-tv-tower
Latitude: 52.52082
Longitude: 13.40942
title: TV Tower
type: LANDMARK
```

Keep generating additional files like these. Pay attention to these constraints:

* All record files end with `.record.yaml`. 
* All record files reside in your data folder **my-data**. It's okay to have sub-folders.

## Compile database

Now it's time to [cure](/glossary.md#curing) your database by running:

```bash
npx cure -v -o output my-data
```

This instructs the ConcreteDB to generate JSON files in folder **output** resembling REST API describing data read from record files in folder **my-data**. The optional `-v` is for being verbose to see the progress of curing your database.

## Test it

By intention, a simple web server is meant to expose the generated files in folder **output** to the public. You can use any web server you like. One option for development setups is [serve](https://www.npmjs.com/package/serve):  

```bash
npx serve -l 8080 output
```

Open your browser at [http://localhost:8080/api/v1/poi.json](http://localhost:8080/api/v1/poi.json) for a list of available points of interest. Single POI with ID `tv-tower` is available at [http://localhost:8080/api/v1/poi/item/tv-tower.json](http://localhost:8080/api/v1/poi/item/tv-tower.json).

Pay attention to some key aspects of provided data:

* All property names are lowercase even though generated data files use mixed variants.
* The variety of property names found in the record files has been fixed, too.
* Some data such as the tags list in resulting database is always an array of lowercase keywords.
