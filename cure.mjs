#!/usr/bin/env node
import File from "fs";
import Path from "path";

import minimist from "minimist";
import YAML from "yaml";

import meta from "./lib/meta.cjs";
import { cure } from "./lib/index.mjs";
import { ShapeFileName } from "./lib/defaults.mjs";
import { YAMLParserOptions } from "./lib/options.mjs";


const args = minimist( process.argv.slice( 2 ) );


if ( args.h || args.help ) {
	console.error( `usage: cure <options> <folder> ...

Cures YAML record files in selected folder(s) into JSON concrete database. 
Processes current folder by default. 

Supported options are:

 -h  /  --help              showing this information
 -V  /  --version           showing version of this package
 -v  /  --verbose           be verbose about the progress of curing database
        --debug             be extra verbose
 -s  /  --shape=file        selects global shape controlling transformation
 -o  /  --output=folder     selects folder resulting files are written to
 -c  /  --clean             remove all existing files from output folder
 -l  /  --library=file      selects file exposing custom term functions to use
                            in addition to regular ones
` );
	process.exit( 0 );
}


if ( args.V || args.version ) {
	console.error( `concrete-db/cure v${meta.version}` );
	process.exit( 0 );
}


( async() => {
	const options = {
		outputFolder: args.o || args.output || process.cwd(),
		verbose: args.debug ? 2 : args.v || args.verbose ? 1 : 0,
		clean: Boolean( args.c || args.clean ),
	};

	const globalShapeFile = args.s || args.shape || Path.resolve( process.cwd(), ShapeFileName );

	const libraries = Array.isArray( args.l ) ? args.l : args.l ? [args.l] : [];
	libraries.splice( libraries.length, 0, ...Array.isArray( args.library ) ? args.library : args.library ? [args.library] : [] );

	options.library = {};
	for ( const library of libraries ) {
		let pathname = Path.resolve( ".", library );

		if ( options.verbose ) {
			console.error( `including library ${pathname}` );
		}

		if ( /^[a-z]:/i.test( pathname ) ) {
			pathname = "file://" + pathname.replace( /\//g, "%2F" );
		}

		const functions = await import( pathname ); // eslint-disable-line no-await-in-loop

		for ( const name of Object.keys( functions ) ) {
			if ( options.verbose > 1 ) {
				console.error( ` - ${name}` );
			}

			options.library[name] = functions[name];
		}
	}

	try {
		options.shape = Object.freeze( YAML.parse( await File.promises.readFile( globalShapeFile, { encoding: "utf8" } ), YAMLParserOptions ) );
	} catch ( cause ) {
		if ( cause.code !== "ENOENT" ) {
			throw cause;
		}
	}


	try {
		await cure( args._.length ? args._ : [process.cwd()], options );
		console.error( "records have been cured properly" );
	} catch ( cause ) {
		console.error( `curing records failed: ${cause.stack}` );
		process.exit( 1 );
	}
} )();
