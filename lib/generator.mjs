import EventEmitter from "events";
import File from "fs";
import Path from "path";

import FileEssentials from "file-essentials";
import PromiseEssentials from "promise-essentials";

import { DefaultGeneratorOptions } from "./defaults.mjs";
import { deepFreeze } from "./helper.mjs";


/**
 * Implements tools for generating files representing provided database.
 *
 * @property {object} options generator customizations
 */
export class Generator extends EventEmitter {
	/**
	 * @param {object} options generator customizations
	 */
	constructor( options = {} ) {
		super();

		Object.defineProperties( this, {
			options: { value: deepFreeze( { ...DefaultGeneratorOptions, ...options } ), enumerable: true },
		} );
	}

	/**
	 * Generates files representing database in selected folder.
	 *
	 * @param {Database} database database to be written
	 * @returns {Promise<Generator>} promises database written
	 */
	async writeDatabase( database ) {
		const { prefix, endpoints } = database;
		const folder = this.options.outputFolder;
		const subfolder = Path.join( folder, prefix );

		await FileEssentials.mkdir( "/", subfolder );

		if ( this.options.clean ) {
			if ( this.options.verbose ) {
				console.error( `cleaning output folder ${subfolder} ...` );
			}

			await FileEssentials.find( subfolder, {
				depthFirst: true,
				filter: ( localPath, fullPath, stat ) => {
					if ( stat.isDirectory() ) {
						return !Path.basename( localPath ).startsWith( "." );
					}

					return stat.isFile();
				},
				converter: ( localPath, fullPath, stat ) => {
					const name = fullPath.replace( /\.json$/, "" );

					if ( !database.hasOwnProperty( name ) ) {
						return stat.isDirectory() ? File.promises.rmdir( fullPath ) : File.promises.unlink( fullPath );
					}

					return undefined;
				},
				waitForConverter: true,
			} );
		}

		const keys = Object.keys( endpoints );
		let index = 0;

		if ( this.options.verbose ) {
			console.error( `writing ${keys.length} files` );
		}

		await PromiseEssentials.each( keys, key => {
			const name = key.endsWith( ".json" ) ? key : key + ".json";
			const path = name.indexOf( "/" ) > -1 ? name.replace( /\/[^/]*?$/, "" ) : undefined;

			if ( this.options.verbose ) {
				process.stderr.write( `\r[${++index}/${keys.length}]` );
			}

			return FileEssentials.mkdir( folder, path )
				.then( () => File.promises.writeFile( Path.resolve( folder, name ), JSON.stringify( endpoints[key] ), { encoding: "utf8" } ) );
		} );

		if ( this.options.verbose ) {
			process.stderr.write( "\n" );
		}

		return this;
	}
}
