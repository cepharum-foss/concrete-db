# The curing process

In ConcreteDB, curing is the process of generating a database from a mostly arbitrary set of YAML-encoded files. It is divided into three major stages:

1. collecting records,
   
2. shaping the database and 
   
5. generating files each representing one of those endpoints.

```mermaid
graph LR
	s1[folder w/ YAML files]
	s2[folder w/ YAML files]
	s3[folder w/ YAML files]
	c{{collector}}
	s{{shaper}}
	g{{generator}}
	d[folder w/ JSON files]
	s1-->c
	s2-->c
	s3-->c
	c-->s
	s-->g
	g-->d
	style c fill:#c30045,stroke:#575757,color:white
	style s fill:#c30045,stroke:#575757,color:white
	style g fill:#c30045,stroke:#575757,color:white
```

## Collector

The collector is iterating over a provided set of base folders deeply searching for YAML-encoded files each defining data of a single [record](/glossary.md#record).

```mermaid
flowchart TD
	start((start))
	folder{another<br>folder?}
	done((end))
	shape[read local shape<br>of folder]
	search[find all record<br>files in folder]
	file{file to<br>read?}
	collect[read record]
	start-->folder
	folder---->|no|done
	folder-->|yes|shape
	shape-->search
	search-->file
	file-->|yes|collect
	file-->|no|folder
	collect-->file
```

Every discovered file is read and associated with a record name derived from its path name and the containing folder's [local shape](/about/shapes.md#local-shapes). The collector stage results in all records read from files grouped by their name.

For example, consider three folders **A**, **B** and **C** containing the following files:

```mermaid
graph TD
	A[folder A]
	a>foo/1.record.yaml]
	b>foo/2.record.yaml]
	c>bar/3.record.yaml]
	A-->a
	A-->b
	A-->c
```

```mermaid
graph TD
	B[folder B]
	d>foo/1.record.yaml]
	e>bar/3.record.yaml]
	B-->d
	B-->e
```

```mermaid
graph TD
	C[folder C]
	f>bar/3.record.yaml]
	C-->f
```

If curing is instructed to collect record files from these folders, the resulting collection of records looks like this:

```mermaid
graph TD
	A(foo/1)
	a[record A/foo/1]
	d[record B/foo/1]
	A-->a
	A-->d
```

```mermaid
graph TD
	B(foo/2)
	b[record A/foo/2]
	B-->b
```

```mermaid
graph TD
	C(bar/3)
	c[record A/bar/3]
	e[record B/bar/3]
	f[record C/bar/3]
	C-->c
	C-->e
	C-->f
```

The top row in either part is representing record name derived from either record's pathname in relation to its originating folder. The bottom row is representing those records.

Let's focus on last part of this chart:

```mermaid
graph TD
	C(bar/3)
	c[record A/bar/3]
	e[record B/bar/3]
	f[record C/bar/3]
	C-->c
	C-->e
	C-->f
```

All three records originate from different folders. Every folder may have a different [local shape](shapes.md#local-shapes) to use on shaping its records. This can be used to extract same kind of data from different kinds of records.
 

## Shaper

The shaping process is the core process of ConcreteDB. It is controlled by the [local shape](shapes.md#local-shapes) which is attached to every collected [record](../glossary.md#record).

The shaping stage is divided into these steps:

1. compiling model items from collected records
2. processing contributions and recompiling all items that have been contributed to
3. creating collections
4. generating endpoints

```mermaid
flowchart LR
	start((start))
	convert[compile]
	contribute[contribute]
	recompile[recompile]
	collections[create collections]
	endpoints[endpoints]
	done((end))
	start-->convert
	convert-->contribute
	contribute-->recompile
	recompile-->collections
	collections-->endpoints
	endpoints-->done
```

### Compiling model items

#### Picking a model

Every record is expected to describe properties of a particular model's item.

The model's name is computed first by evaluating the term in [meta property `$model`](/api/shapes.md#model) of [common shape](shapes.md#common-shape) valid in context of either record. After that, the record's [model shape](shapes.md#model-shape) is compiled by merging it with common shape to control further steps described below.

```mermaid
graph TD
	A(foo/1)
	B(foo/2)
	a[record A/foo/1]
	b[record A/foo/2]
	d[record B/foo/1]
	A-->a
	A-->d
	B-->b
	FOO{{foo}}
	a-->FOO
	b-->FOO
	d-->FOO
```

```mermaid
graph TD
	C(bar/3)
	c[record A/bar/3]
	e[record B/bar/3]
	f[record C/bar/3]
	C-->c
	C-->e
	C-->f
	BAR{{bar}}
	c-->BAR
	e-->BAR
	f-->BAR
```

Models can be derived from either record's name e.g. by picking its second to last segment, which is either `foo` or `bar` in this example. You can also use a property given in either record for picking its model. Finally, you could inject and use a custom library of term functions implementing your own magic way of associating a record with a model.

The definition of model detection must be given as part of [common shape](../api/shapes.md#common). In this example, its essential part might look like this:

```yaml
common:
  properties:
    $id: uncollect( slice( $segments, -2 ) )
```

#### Picking an ID

Next, every record must identify the item it is describing. Therefore, the term of [meta property `$id`](/api/shapes.md#id) is evaluated in context of either record to compute the item's ID.

```mermaid
graph TD
	A(foo/1)
	B(foo/2)
	a[record A/foo/1]
	b[record A/foo/2]
	d[record B/foo/1]
	A-->a
	A-->d
	B-->b
	1((1))
	2((2))
	a-->1
	b-->2
	d-->1
```

```mermaid
graph TD
	C(bar/3)
	c[record A/bar/3]
	e[record B/bar/3]
	f[record C/bar/3]
	C-->c
	C-->e
	C-->f
	3((3))
	c-->3
	e-->3
	f-->3
```

Similar to picking a model, there are multiple ways for picking the ID of a model's item. In this example, last segment of either record's name is used. The essential part of the model shape might look like this:

```yaml
models:
  foo:
    properties:
      $id: uncollect( slice( $segments, -1 ) )
```

Basically, every record is describing one item of a model. However, multiple records are capable of describing the same item (as illustrated below). This can be used to implement variants per item or simply have different sources defining your items with the curing process merging all those sources into a single item eventually. Merging happens in same order of records as collected before. That's why order of processing folders during collection stage is important.

Combining picked model and ID for every collected record results in either record's related item to be generated in resulting database:

```mermaid
graph TD
	A(foo/1)
	B(foo/2)
	a[record A/foo/1]
	b[record A/foo/2]
	d[record B/foo/1]
	A-->a
	A-->d
	B-->b
	FOO1[[foo #1]]
	FOO2[[foo #2]]
	a-->FOO1
	b-->FOO2
	d-->FOO1
```

```mermaid
graph TD
	C(bar/3)
	c[record A/bar/3]
	e[record B/bar/3]
	f[record C/bar/3]
	C-->c
	C-->e
	C-->f
	BAR3[[bar #3]]
	c-->BAR3
	e-->BAR3
	f-->BAR3
```

#### Compiling item properties

Properties of items aren't just copies of related record's properties but computed based on a [set of rules](/api/shapes.md#properties) either [model shape](/api/shapes.md#model-shape) is defining.

As illustrated above, multiple records can describe the same item, so processing them is affecting that item multiple times. [Defaults](/api/shapes.md#defaults) apply on processing first record, only. Additionally processed records augment or adjust the addressed item.

:::tip On merging multiple records
If multiple records address the same item of a model, the following merging strategy applies:

* If computed property value is nullish, nothing is done. Any previous value of selected item is kept as-is. On first record, the property isn't set.
* By default, a non-nullish value of a record is replacing any value declared by a previously processed record.
* A shape can declare [collecting properties](../api/shapes.md#collecting-properties). For those, any computed value is collected in addition to previously computed values of the same property. Any such property ends up with an array of values.
:::

#### Data validation

The step of compiling item properties is additionally suitable for validating any data. Using [special term function `fail()`](../api/functions.md#fail), any computation of a property value can intentionally fail the whole curing process.

```yaml
models:
  foo:
    properties:
      title: first( title, fail( "missing title of foo" ) )
```

Injecting this example into a model shape, the curing process is failing each time a single record doesn't have a title.

### Merging local shapes

:::tip Some valuable insight
Actually, this isn't a dedicated step of shaping stage, but it might be crucial to understand how local shapes are used and how they evolve over time.
:::

Every collected record is compiled into a (partial) item of a model as described before. For every record, the local shape of its originating folder is essential to picking a model's item and to compiling its properties. This way, different data structures can be processed in different ways to get a commonly structured database.

But as soon as all collected records have been compiled into items of models, those items loose any close relationship to a particular folder and its local shape. That's because multiple records from different folders might have been involved in compiling the resulting item based on different local shapes. In addition, the steps described below may regard models that haven't been addressed by any record read from either folder before at all and thus don't come with a local shape either.

That's why all local shapes of either processed folder are deeply merged in same order of processing the folders on collecting records.

Keeping that in mind, you should consider these suggestions on importing data from multiple folders with different local shapes:

* A local shape should focus on describing properties per model to be gained from related folder.

* Collections and contributions should be defined in a global shape which is provided as option on invoking curing process.

  ```bash
  cure -s path/to/global.shape.yaml -o output sourceFolder1 sourceFolder2
  ```

### Processing contributions <Badge text="v0.2.0+"></Badge>

[Contributions](../glossary.md#contribution) are processed after compiling all items based on collected records. Their definition per model is read from recent merge of local shapes.

:::tip What's the benefit of contributions?
Relational databases support queries for grouping data and for delivering computed information on those groups of data. You can e.g. group a table of books by their author and then get _the number of books per author_ as computed information.

Contributions are ConcreteDB's approach to achieve similar results. Regular terms per property aren't capable of that.
:::

For example, consider having several records each describing a book including its author. After compiling items there are several items of model `book`:

```mermaid
graph TD
	B1[[book #1 with author John Doe]]
	B2[[book #2 with author Jane Doe]]
	B3[[book #3 with author John Doe]]
```

The model shape of `book` can declare, that either item is _contributing_ to model `author` by selecting an item of `author` for contribution and describing property values to merge with selected item of model `author` in a similar way as done before on converting records into items of model `book`:

```yaml
models:
  book:
    contributions:
      author:
        $id: slugify( author )
        books[]: $id
```

* Every model can contribute to multiple models.
* Contributions of a model (here: `book`) are declared per model that's contributed to (here: `author`).
* Special property `$id` is computed to select item to contribute to. Here, a slug derived from author's name is used.
* All other properties are used to compile values per property to contribute. By declaring [collecting properties](../api/shapes.md#collecting-properties), contributions from different items are collected by selected target item.
* Contributions are computed based on either contributing item. Thus, `$id` in last line of example given before is referring to the contributing book's ID.

This would result in items like these:

```mermaid
graph TD
	B1[[book #1 with author John Doe]]
	B3[[book #3 with author John Doe]]
	Am[Author John Doe]
	Am-->B1
	Am-->B3
```

```mermaid
graph TD
	B2[[book #2 with author Jane Doe]]
	Af[Author Jane Doe]
	Af-->B2
```

The resulting database is covering models read from collected records and those based on contributions made by other models.

:::tip Advanced feature
Either contribution may address multiple items of a target model. This is achieved by delivering a whole list of IDs instead of a single ID. 

This feature is essential for grouping record-based items by properties with multiple values such as a keyword index based on a list of keywords per item. 

Last but not least, it can be used to establish models supporting a client-side search engine based on term function [`terms()`](../api/functions.md#terms).
:::

### Recompiling items with contributions

To eventually support computations based on grouped data as mentioned before, either item with contributions by other items is recompiled based on its properties. A separate meta property `$post` is set to indicate this.

Following example given before, the number of contributing books can be computed as a dedicated property of items of model `author`.

```mermaid
graph TD
	Am[Author John Doe]
	Af[Author Jane Doe]
	C1(number of books: 1)
	C2(number of books: 2)
	Am-->C2
	Af-->C1
```

### Creating collections

At this point, the curing process knows all items per model to expose by either item's ID.

```mermaid
graph TD
	MB{{model<br>book}}
	B1[[book #1]]
	B2[[book #2]]
	B3[[book #3]]
	B4[[book #4]]
	B5[[book #5]]
	MB-->B1
	MB-->B2
	MB-->B3
	MB-->B4
	MB-->B5
```

```mermaid
graph TD
	MA{{model<br>author}}
	A1[[author #1]]
	A2[[author #2]]
	A3[[author #3]]
	MA-->A1
	MA-->A2
	MA-->A3
```

```mermaid
graph TD
	MG{{model<br>genre}}
	G1[[genre #1]]
	G2[[genre #2]]
	MG-->G1
	MG-->G2
```

This is the substantial part of the [database](../glossary.md#database) eventually resulting from the curing process. But it's still lacking [collections](../glossary.md#collection) based on those items. Collections are intended for convenient consumption of data e.g. to present lists of items per model for browsing or to enable a client to discover available items.

For every [model](/glossary.md#model), its [model shape](/api/shapes.md#model-shape) defines zero or more [collections](/api/shapes.md#collections) of [items](/glossary.md#item). This includes [regular](../api/collections.md#regular-collections) and [keyed](../api/collections.md#keyed-collections) collections. Every collection is solely based on those items compiled before representing a certain subset of those items using [a dedicated set of derived properties](../api/collections.md#properties) optionally applying [grouping](../api/collections.md#keyed-collections), [filtering](../api/collections.md#filter), [sorting](../api/collections.md#sort) and/or [slicing](../api/collections.md#slicing).

:::tip Example
After compiling 100 items of model `book`, several collections can be created e.g. to:

* unconditionally list [title and ID](../api/collections.md#properties) of all books omitting either book's year of publication,
* list title, author and ID of all books [sorted by author](../api/collections.md#sort) once in ascending and once in descending order,
* list title and ID of books [per author or per genre](../api/collections.md#keyed-collections),
* list title and ID of books [that have been published this year](../api/collections.md#filter),
* list all books [divided into chunks of 20 books each](../api/collections.md#slicing).
:::

Every such customization to a set of items to deliver requires another collection defined in either [model's shape](shapes.md#model-shape). For example, you have to declare three separate collections to get

* a list of books sorted by title,
* the same list of books sorted by author's name any
* the same list of books sorted by year of publication.

This might look very inconvenient at first glance, but the support for a [hierarchy of shapes](shapes.md#sources-of-shapes) and YAML's [node anchors and references](https://en.wikipedia.org/wiki/YAML#Advanced_components) are available to mitigate the risk of getting lost in a growing complexity of shape definitions. 

:::tip Automatic index creation
By default, the [common shape](shapes.md#common-shape) is defining an unconditional regular collection for every model. That's why all cured databases automatically include lists of items per model unless disabled explicitly.
:::


### Creating endpoints

All items and all collections of items are turned into [endpoints](../glossary.md#endpoint) by assigning [routes](../glossary.md#route). Either route has to provide all information required to distinguish endpoints from each other. This includes certain parameters such as the ID of an item to provide or the offset of a chunk of items in a [sliced collection](../api/collections.md#slicing).

:::tip URL query support
URL queries are **not supported** as parameters.

* The idea of ConcreteDB is to create a set of files that resemble the REST API of a server-side database providing read access, only. Thus, either route must be convertible into an actual file's name. Using question mark in file names may cause issues.

* A URL query may consist of key-value assignments in _arbitrary_ combination and order. Supporting that in ConcreteDB requires every possible combination and order of query parameters to be prepared in advance. The hierarchy of files resulting from that would be highly redundant consuming a multitude of disk space without any significant benefit.

You should consider using dynamic rewriting as supported by [major](https://httpd.apache.org/docs/2.4/rewrite/intro.html) [web](http://nginx.org/en/docs/http/ngx_http_rewrite_module.html#rewrite) [server](https://doc.lighttpd.net/lighttpd2/mod_rewrite.html) implementations if your application fails to use the path-only approach of ConcreteDB.
:::

#### Item endpoints

For every item per model, an [endpoint](/glossary.md#endpoint) with a fixed [route](/about/routes.md#managed-routes) is created to provide either item's data:

```mermaid
graph LR
	R[/ /api/v1/book/item/1 /]
	B[[book #1]]
	R-->B
```

```mermaid
graph LR
	R[/ /api/v1/book/item/2 /]
	B[[book #2]]
	R-->B
```

```mermaid
graph LR
	R[/ /api/v1/book/item/3 /]
	B[[book #3]]
	R-->B
```

```mermaid
graph LR
	R[/ /api/v1/book/item/4 /]
	B[[book #4]]
	R-->B
```

```mermaid
graph LR
	R[/ /api/v1/book/item/5 /]
	B[[book #5]]
	R-->B
```

```mermaid
graph LR
	R[/ /api/v1/author/item/1 /]
	B[[author #1]]
	R-->B
```

```mermaid
graph LR
	R[/ /api/v1/author/item/2 /]
	B[[author #2]]
	R-->B
```

```mermaid
graph LR
	R[/ /api/v1/author/item/3 /]
	B[[author #3]]
	R-->B
```

```mermaid
graph LR
	R[/ /api/v1/genre/item/1 /]
	B[[genre #1]]
	R-->B
```

```mermaid
graph LR
	R[/ /api/v1/genre/item/2 /]
	B[[genre #2]]
	R-->B
```

By default, the `/api/v1` part is the _global prefix_ for the resulting database. It can be customized on invoking curing process. The following three segments are 

* the model's name, e.g. `book` or `author`, 
* the special keyword `item` and 
* the ID of either exposed item.

Those three segments aren't customizable as of now.

#### Regular collection endpoints

A [regular collection](../api/collections.md#regular-collections) is represented by a single endpoint, by default. The endpoint gets associated with a [custom route](/about/routes.md#custom-routes) as defined in [model shape](shapes.md#model-shape) which is appended after the model's name. 

That custom route may be empty. In fact, the [common shape](shapes.md#common-shape) defines an unconditional regular collection for every model by default using an empty custom route.

```yaml
common:
  collections:
    "": {}
```

This results in endpoints like these:

```mermaid
graph LR
	R[/ /api/v1/book /]
	B[[book #1<br>book #2<br>book #3<br>book #4<br>book#4<br>book #5]]
	R-->B
```

```mermaid
graph LR
	R[/ /api/v1/author /]
	B[[author #1<br>author #2<br>author #3]]
	R-->B
```

```mermaid
graph LR
	R[/ /api/v1/genre /]
	B[[genre #1<br>genre #2]]
	R-->B
```

#### Keyed collection endpoints

For example, an additionally defined [keyed collection](../api/collections.md#keyed-collections) has an index endpoint, by default:

```mermaid
graph LR
	R[/ /api/v1/book/by/genre /]
	B[['foo' has x matches<br>'bar' has y matches]]
	R-->B
```

Again, `/api/v1` is the global prefix and `book` is the model's name. `by/genre` has been given in shape as a custom route of this collection.

In addition to that index endpoint, another endpoint is created for every available value of selected key to list model items related to it. Either value gets appended to the custom route, by default.

```mermaid
graph LR
	R[/ /api/v1/book/by/genre/foo /]
	B[[1st book of genre 'foo'<br>2nd book of genre 'foo'<br>...<br>xth book of genre 'foo']]
	R-->B
```

```mermaid
graph LR
	R[/ /api/v1/book/by/genre/bar /]
	B[[1st book of genre 'bar'<br>2nd book of genre 'bar'<br>...<br>yth book of genre 'bar']]
	R-->B
```

#### Slicing

For [sliced collections](../api/collections.md#slicing), a number of endpoints depending on the amount of items per collection is created. 

For example, if a collection of model `book` is sliced into chunks of 20 items each, a single endpoint is created if 17 items of model `book` have been compiled before:

```mermaid
graph LR
	R[/ /api/v1/book/offset/0/limit/20 /]
	B[[book #1<br>book #2<br>...<br>book #17]]
	R-->B
```

However, if collecting records and compiling items yielded 55 items of model `book`, three separate endpoints for that sliced collection are created:

```mermaid
graph LR
	R[/ /api/v1/book/offset/0/limit/20 /]
	B[[book #1<br>book #2<br>...<br>book #20]]
	R-->B
```

```mermaid
graph LR
	R[/ /api/v1/book/offset/20/limit/20 /]
	B[[book #21<br>book #22<br>...<br>book #40]]
	R-->B
```

```mermaid
graph LR
	R[/ /api/v1/book/offset/40/limit/20 /]
	B[[book #41<br>book #42<br>...<br>book #55]]
	R-->B
```

## Generator

The generator stage is rather straightforward. The whole [database](../glossary.md#database) - which is a set of [endpoints](../glossary.md#endpoint) - is written to a set of JSON-encoded files in selected output folder.

Names of files to write are derived by appending extension `.json` to either endpoint's route:


```mermaid
graph LR
	R[/ /api/v1/book /]
	F> output/folder/api/v1/book.json ]
	R --> F
```

```mermaid
graph LR
	R[/ /api/v1/book/item/12 /]
	F> output/folder/api/v1/book/item/12.json ]
	R --> F
```

```mermaid
graph LR
	R[/ /api/v1/book/offset/0/limit/20 /]
	F> output/folder/api/v1/book/offset/0/limit/20.json ]
	R --> F
```

Any sub-folder to the output folder that is required to contain a named file is created implicitly.

This way all files in output folder eventually resemble the hierarchy of routes and can be integrated with an major web server pretty easily.
