import { describe, it } from "mocha";
import "should";

import { spread } from "../../lib/term-functions.mjs";


describe( "Statistical function `spread`", () => {
	it( "is a function", () => {
		spread.should.be.a.Function();
	} );

	it( "delivers empty list on processing non-string scalar input by default", () => {
		spread().should.be.an.Object().which.is.empty();
		spread( undefined ).should.be.an.Object().which.is.empty();
		spread( null ).should.be.an.Object().which.is.empty();
		spread( false ).should.be.an.Object().which.is.empty();
		spread( true ).should.be.an.Object().which.is.empty();
		spread( 0 ).should.be.an.Object().which.is.empty();
		spread( 100000.3456789 ).should.be.an.Object().which.is.empty();
		spread( -100000.3456789 ).should.be.an.Object().which.is.empty();
		spread( () => "some string result" ).should.be.an.Object().which.is.empty();
	} );

	it( "delivers count per stringified scalar input on demand", () => {
		spread( undefined, 1, 1, Infinity, false, false ).should.be.an.Object().which.is.deepEqual( { undefined: 1 } );
		spread( null, 1, 1, Infinity, false, false ).should.be.an.Object().which.is.deepEqual( { null: 1 } );
		spread( false, 1, 1, Infinity, false, false ).should.be.an.Object().which.is.deepEqual( { false: 1 } );
		spread( true, 1, 1, Infinity, false, false ).should.be.an.Object().which.is.deepEqual( { true: 1 } );
		spread( 0, 1, 1, Infinity, false, false ).should.be.an.Object().which.is.deepEqual( { 0: 1 } );
		spread( 100000.3456789, 10, 1, Infinity, false, false ).should.be.an.Object().which.is.deepEqual( { 100000: 1, 3456789: 1 } );
		spread( -100000.3456789, 10, 1, Infinity, false, false ).should.be.an.Object().which.is.deepEqual( { 100000: 1, 3456789: 1 } );
		spread( () => "some string result", 10, 1, Infinity, false, false ).should.be.an.Object().which.is.deepEqual( { some: 1, string: 1, result: 1 } );
	} );

	it( "delivers empty list on processing too short string scalar input by default", () => {
		spread( "" ).should.be.an.Object().which.is.empty();
		spread( "foo" ).should.be.an.Object().which.is.empty();
		spread( "               bar             " ).should.be.an.Object().which.is.empty();
	} );

	it( "delivers counts per consecutive sequences of letters and digits found in a sufficiently complex string scalar input by default", () => {
		spread( "superduper" ).should.be.an.Object().which.has.size( 1 ).and.containDeep( { superduper: 1 } );
		spread( "super super" ).should.be.an.Object().which.has.size( 1 ).and.containDeep( { super: 2 } );
		spread( "super - duper: Super" ).should.be.an.Object().which.has.size( 2 ).and.containDeep( { super: 2, duper: 1 } );
		spread( "(a) quick brown Ground-hog _jumps_ 'over' some lazy frog.", 10, 3, Infinity, false ).should.be.an.Object().which.has.size( 8 ).and.containDeep( {
			quick: 1,
			brown: 1,
			"Ground-hog": 1,
			_jumps_: 1,
			over: 1,
			some: 1,
			lazy: 1,
			frog: 1,
		} );
	} );

	it( "recursively extracts counts of terms from sufficiently complex string properties of an object by default", () => {
		const data = {
			age: 36,
			action: () => "welcome to the demo",
			title: "Complex data for demonstration",
			context: {
				ready: true,
				version: 10000.3456789,
				label: "The foo",
				teaser: "A demonstration of possible use-cases.",
				tags: [
					"demo", "complex", "non-scalar", () => "to be processed",
				],
			},
		};

		spread( data ).should.be.an.Object().which.is.deepEqual( {
			complex: 1,
			data: 1,
			for: 1,
			demonstration: 2,
			possible: 1,
			"use-cases": 1,
			"non-scalar": 1,
		} );

		// lower required complexity per string to get more terms
		spread( data, 1, 1, 30, false ).should.be.an.Object().which.is.deepEqual( {
			Complex: 1,
			data: 1,
			for: 1,
			demonstration: 2,
			The: 1,
			foo: 1,
			A: 1,
			of: 1,
			possible: 1,
			"use-cases": 1,
			demo: 1,
			complex: 1,
			"non-scalar": 1,
		} );

		// considers selected complexity per word
		spread( data, 1, 3 ).should.be.an.Object().which.is.deepEqual( {
			complex: 2,
			data: 1,
			for: 1,
			demonstration: 2,
			the: 1,
			foo: 1,
			possible: 1,
			"use-cases": 1,
			demo: 1,
			"non-scalar": 1,
		} );

		// ignores case on demand
		spread( data, 1, 1, Infinity, false ).should.be.an.Object().which.is.deepEqual( {
			Complex: 1,
			complex: 1,
			data: 1,
			for: 1,
			demonstration: 2,
			The: 1,
			foo: 1,
			A: 1,
			of: 1,
			possible: 1,
			"use-cases": 1,
			demo: 1,
			"non-scalar": 1,
		} );

		// considers stringified properties on demand, too
		spread( data, 1, 1, Infinity, true, false ).should.be.an.Object().which.is.deepEqual( {
			36: 1,
			welcome: 1,
			to: 2,
			the: 2,
			demo: 2,
			complex: 2,
			data: 1,
			for: 1,
			demonstration: 2,
			true: 1,
			10000: 1,
			3456789: 1,
			foo: 1,
			a: 1,
			of: 1,
			possible: 1,
			"use-cases": 1,
			"non-scalar": 1,
			be: 1,
			processed: 1,
		} );
	} );

	it( "accepts custom pattern for extracting words", () => {
		const data = {
			age: 36,
			action: () => "welcome to the demo",
			title: "Complex data for demonstration",
			context: {
				ready: true,
				version: 10000.3456789,
				label: "The foo",
				teaser: "A demonstration of possible use-cases.",
				tags: [
					"demo", "complex", "non-scalar", () => "to be processed",
				],
			},
		};

		spread( data, 10, 3, Infinity, true, true ).should.be.an.Object().which.is.deepEqual( {
			complex: 1,
			data: 1,
			for: 1,
			demonstration: 2,
			possible: 1,
			"use-cases": 1,
			"non-scalar": 1,
		} );

		spread( data, 10, 3, Infinity, true, true, "[\\p{L}\\p{N}]+" ).should.be.an.Object().which.is.deepEqual( {
			complex: 1,
			data: 1,
			for: 1,
			demonstration: 2,
			possible: 1,
			use: 1,
			cases: 1,
			non: 1,
			scalar: 1
		} );

		spread( data, 10, 3, Infinity, true, true, "[\\p{L}\\p{N} _-]+" ).should.be.an.Object().which.is.deepEqual( {
			"complex data for demonstration": 1,
			"a demonstration of possible use-cases": 1,
			"non-scalar": 1,
		} );
	} );
} );
