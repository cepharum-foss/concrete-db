export * from "simple-terms/lib/helper.js";

/**
 * Wraps provided object in a proxy offering case-insensitive access on
 * object's properties. Any accessed property's value is wrapped before
 * returning as well.
 *
 * Any other kind of data is returned as-is.
 *
 * @param {any} data data to wrap
 * @param {object} meta set of meta properties to blend in (case-sensitively)
 * @returns {any} provided data wrapped for case-insensitive selection of own properties
 */
export function augmentDataSpace( data, meta = undefined ) {
	if ( !data || typeof data !== "object" ) {
		return data;
	}

	let cached;

	return new Proxy( data, {
		get: ( object, name ) => {
			if ( meta && typeof name === "string" && name.startsWith( "$" ) ) {
				return meta[name];
			}

			if ( typeof name === "symbol" ) {
				return object[name];
			}

			if ( Object.prototype.hasOwnProperty.call( object, name ) ) {
				return augmentDataSpace( object[name] );
			}

			if ( !cached && typeof name === "string" ) {
				cached = {};

				for ( const usedName of Object.keys( object ) ) {
					const lowercaseName = usedName.toLowerCase();

					if ( Object.prototype.hasOwnProperty.call( cached, lowercaseName ) ) {
						console.warn( `ambiguous property naming: found ${usedName} and ${cached[lowercaseName]}` );
					} else {
						cached[lowercaseName] = usedName;
					}
				}
			}

			return augmentDataSpace( object[cached[name.toLocaleLowerCase()]] );
		},
	} );
}

/**
 * Implements special kind of value representing reference on uniquely identified
 * item of a model selected by its name.
 *
 * @property {string} model name of model referenced item belongs to
 * @property {string|number} id unique identifier of referenced item in context of its model
 */
export class ForeignKey {
	/**
	 * @param {string} model name of referenced item's model
	 * @param {string|number} id unique identifier of referenced item
	 */
	constructor( model, id ) {
		Object.defineProperties( this, {
			model: { value: model, enumerable: true },
			id: { value: id, enumerable: true },
		} );
	}

	/**
	 * Represents foreign key as simple string.
	 *
	 * @returns {string} foreign key represented as string
	 */
	toString() {
		return `${this.model}/${this.id}`;
	}

	/**
	 * Fetches referenced item from provided pool of items per named model.
	 *
	 * @param {DatabaseCollection} models pools of model names into map of item IDs into items
	 * @returns {string} foreign key represented as string
	 */
	pickFrom( models ) {
		return models?.[this.model]?.[this.id];
	}
}

/**
 * Deeply freezes provided object and all its properties recursively.
 *
 * @param {object} object object to freeze deeply; will be frozen on return, too
 * @returns {object} deeply frozen object
 */
export function deepFreeze( object ) {
	// FIXME implement actually deep freezing of provided object
	return Object.freeze( object );
}
