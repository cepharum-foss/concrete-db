import { readFileSync } from "fs";
import { dirname, resolve } from "path";
import { fileURLToPath } from "url";

import YAML from "yaml";

export const ShapeFileName = ".concrete-db.shape.yaml";

/**
 * @type {CollectorOptions}
 */
export const DefaultCollectorOptions = {
	/**
	 * Matches names of files to collect.
	 */
	pattern: /\.record\.ya?ml$/i,

	/**
	 * Derives key of a record from local path name of file it is originating
	 * from.
	 *
	 * @param {string} name local path name of file containing a record
	 * @returns {string} key of record to use in further processing
	 */
	nameToKey: name => name.replace( /\.record\.ya?ml$/i, "" ),
};

/**
 * @type {ShaperOptions}
 */
export const DefaultShaperOptions = {
	prefix: "/api/v1",
};

/**
 * @type {GeneratorOptions}
 */
export const DefaultGeneratorOptions = {};

/**
 * @type {Shape}
 */
export const DefaultShape = YAML.parse( readFileSync( resolve( dirname( fileURLToPath( import.meta.url ) ), "default.shape.yaml" ), { encoding: "utf8" } ) );
