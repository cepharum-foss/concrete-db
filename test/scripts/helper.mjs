import { Collector, Generator } from "../../lib/index.mjs";
import merge from "lodash.merge";
import { DefaultShape } from "../../lib/defaults.mjs";

/**
 * Exposes provided collection of sources instead of actually enumerating file
 * system for record files.
 */
export class FakeCollector extends Collector {
	constructor( collectedSource, localShape, options = {} ) {
		super( options );

		for ( const key of Object.keys( collectedSource ) ) {
			this.records.set( key, collectedSource[key] );
		}

		if ( this.records.size && !localShape ) {
			throw new Error( "missing fake local shape" );
		}

		this.localShapes.push( merge( {}, DefaultShape, localShape ) );
	}

	fromFolders() { return this; }
}

/**
 * Grabs database to be written as part of curing process without actually
 * writing it anywhere.
 */
export class FakeGenerator extends Generator {
	writeDatabase( database ) {
		this.writtenDatabase = database;
	}
}

/**
 * Extends default shape with a model-specific part.
 *
 * @param {string} modelName name of model to shape
 * @param {ShapeModel} modelShape shape of named model
 * @param {ShapeModel} commonShape common shape to use as fallback for all models
 * @returns {Shape} resulting shape
 */
export function fakeModelShape( modelName, modelShape, commonShape = {} ) {
	return merge( {}, DefaultShape, { common: commonShape }, {
		models: {
			[modelName]: modelShape,
		},
	} );
}

/**
 * Fakes collector's qualification of records.
 *
 * @param {Object<string,object>} namedRecords maps names of record into their content
 * @param {Shape} localShape local shape of attached to qualified records
 * @returns {CollectedSources} collected records grouped by their name
 */
export function qualifyFakedRecords( namedRecords, localShape ) {
	const qualified = {};

	for ( const name of Object.keys( namedRecords ) ) {
		let records = namedRecords[name];
		if ( !Array.isArray( records ) ) {
			records = [records];
		}

		qualified[name] = records.map( record => ( {
			folder: "$fake",
			segments: name.split( "/" ),
			shape: localShape,
			data: record,
		} ) );
	}

	return Object.defineProperties( qualified, {
		$shape: { value: localShape }
	} );
}
