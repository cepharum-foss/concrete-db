import { describe, it } from "mocha";
import "should";
import merge from "lodash.merge";

import { cure } from "../../lib/index.mjs";
import { FakeCollector, FakeGenerator, fakeModelShape, qualifyFakedRecords } from "./helper.mjs";


describe( "Curing: creating sliced collection", () => {
	it( "ignores slicing marker with incomplete argument", async() => {
		const generator = new FakeGenerator();
		const source = data( {
			common: { collections: { "{limit:}": {} } },
		} );
		const collector = new FakeCollector( source, source.$shape );

		await cure( [], {}, { collector, generator } );

		generator.writtenDatabase.endpoints["api/v1/event/{limit:}"].should.be.Object().which.has.properties( "count", "items" );
		generator.writtenDatabase.endpoints["api/v1/event/{limit:}"].items.should.be.Array().which.has.length( 45 );
	} );

	it( "ignores slicing marker with invalid argument", async() => {
		const generator = new FakeGenerator();
		const source = data( {
			common: { collections: { "{limit:a}": {} } },
		} );
		const collector = new FakeCollector( source, source.$shape );

		await cure( [], {}, { collector, generator } );

		generator.writtenDatabase.endpoints["api/v1/event/{limit:a}"].should.be.Object().which.has.properties( "count", "items" );
		generator.writtenDatabase.endpoints["api/v1/event/{limit:a}"].items.should.be.Array().which.has.length( 45 );
	} );

	it( "generates separate endpoints per sliced regular collection", async() => {
		const generator = new FakeGenerator();
		const source = data();
		const collector = new FakeCollector( source, source.$shape );

		await cure( [], {}, { collector, generator } );

		const { endpoints } = generator.writtenDatabase;

		// due to collection defined in default global shape:
		endpoints["api/v1/event"].items.should.be.Array().which.has.length( 45 );

		// due to collections defined in local shape:
		endpoints["api/v1/event/0"].count.should.be.Number().which.is.equal( 45 );
		endpoints["api/v1/event/0"].items.should.be.Array().which.has.length( 10 );
		endpoints["api/v1/event/0"].items.map( r => r.index ).join( "," ).should.be.equal( "1,2,3,4,5,6,7,8,9,10" );
		endpoints["api/v1/event/10"].count.should.be.Number().which.is.equal( 45 );
		endpoints["api/v1/event/10"].items.should.be.Array().which.has.length( 10 );
		endpoints["api/v1/event/10"].items.map( r => r.index ).join( "," ).should.be.equal( "11,12,13,14,15,16,17,18,19,20" );
		endpoints["api/v1/event/20"].count.should.be.Number().which.is.equal( 45 );
		endpoints["api/v1/event/20"].items.should.be.Array().which.has.length( 10 );
		endpoints["api/v1/event/20"].items.map( r => r.index ).join( "," ).should.be.equal( "21,22,23,24,25,26,27,28,29,30" );
		endpoints["api/v1/event/30"].count.should.be.Number().which.is.equal( 45 );
		endpoints["api/v1/event/30"].items.should.be.Array().which.has.length( 10 );
		endpoints["api/v1/event/30"].items.map( r => r.index ).join( "," ).should.be.equal( "31,32,33,34,35,36,37,38,39,40" );
		endpoints["api/v1/event/40"].count.should.be.Number().which.is.equal( 45 );
		endpoints["api/v1/event/40"].items.should.be.Array().which.has.length( 5 );
		endpoints["api/v1/event/40"].items.map( r => r.index ).join( "," ).should.be.equal( "41,42,43,44,45" );

		// missing due to lacking explicit definition in local shape
		( endpoints["api/v1/event/limit/20/offset"] == null ).should.be.true();

		// due to collections defined in local shape:
		endpoints["api/v1/event/limit/20/offset/0"].count.should.be.Number().which.is.equal( 45 );
		endpoints["api/v1/event/limit/20/offset/0"].items.should.be.Array().which.has.length( 20 );
		endpoints["api/v1/event/limit/20/offset/0"].items.map( r => r.index ).join( "," ).should.be.equal( "1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20" );
		endpoints["api/v1/event/limit/20/offset/20"].count.should.be.Number().which.is.equal( 45 );
		endpoints["api/v1/event/limit/20/offset/20"].items.should.be.Array().which.has.length( 20 );
		endpoints["api/v1/event/limit/20/offset/20"].items.map( r => r.index ).join( "," ).should.be.equal( "21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40" );
		endpoints["api/v1/event/limit/20/offset/40"].count.should.be.Number().which.is.equal( 45 );
		endpoints["api/v1/event/limit/20/offset/40"].items.should.be.Array().which.has.length( 5 );
		endpoints["api/v1/event/limit/20/offset/40"].items.map( r => r.index ).join( "," ).should.be.equal( "41,42,43,44,45" );
	} );

	it( "rejects slicing a keyed collection w/o keyed routes", async() => {
		const generator = new FakeGenerator();
		const source = data( {
			common: { collections: { "keyed/{limit:10}": { key: "type" } } },
		} );
		const collector = new FakeCollector( source, source.$shape );

		await cure( [], {}, { collector, generator } ).should.be.rejectedWith( /{key}/ );
	} );

	it( "generates separate endpoints per sliced regular collection embedded in keyed collection w/ keyed routes", async() => {
		const generator = new FakeGenerator();
		const source = data();
		const collector = new FakeCollector( source, source.$shape );

		await cure( [], {}, { collector, generator } );

		const { endpoints } = generator.writtenDatabase;

		// missing due to lacking explicit definition in local shape
		( endpoints["api/v1/event/keyed"] == null ).should.be.true();
		( endpoints["api/v1/event/keyed/info"] == null ).should.be.true();
		( endpoints["api/v1/event/keyed/error"] == null ).should.be.true();

		// due to collections defined in local shape:
		endpoints["api/v1/event/keyed/info/0"].count.should.be.Number().which.is.equal( 28 );
		endpoints["api/v1/event/keyed/info/0"].items.should.be.Array().which.has.length( 10 );
		endpoints["api/v1/event/keyed/info/0"].items.map( r => r.index ).join( "," ).should.be.equal( "5,6,7,8,9,10,11,12,13,14" );
		endpoints["api/v1/event/keyed/info/10"].count.should.be.Number().which.is.equal( 28 );
		endpoints["api/v1/event/keyed/info/10"].items.should.be.Array().which.has.length( 10 );
		endpoints["api/v1/event/keyed/info/10"].items.map( r => r.index ).join( "," ).should.be.equal( "15,16,17,18,19,20,21,22,23,24" );
		endpoints["api/v1/event/keyed/info/20"].count.should.be.Number().which.is.equal( 28 );
		endpoints["api/v1/event/keyed/info/20"].items.should.be.Array().which.has.length( 8 );
		endpoints["api/v1/event/keyed/info/20"].items.map( r => r.index ).join( "," ).should.be.equal( "25,26,27,28,29,30,31,32" );

		endpoints["api/v1/event/keyed/error/0"].count.should.be.Number().which.is.equal( 17 );
		endpoints["api/v1/event/keyed/error/0"].items.should.be.Array().which.has.length( 10 );
		endpoints["api/v1/event/keyed/error/0"].items.map( r => r.index ).join( "," ).should.be.equal( "1,2,3,4,33,34,35,36,37,38" );
		endpoints["api/v1/event/keyed/error/10"].count.should.be.Number().which.is.equal( 17 );
		endpoints["api/v1/event/keyed/error/10"].items.should.be.Array().which.has.length( 7 );
		endpoints["api/v1/event/keyed/error/10"].items.map( r => r.index ).join( "," ).should.be.equal( "39,40,41,42,43,44,45" );

		// missing due to lacking explicit definition in local shape
		( endpoints["api/v1/event/keyed/info/limit/25/offset"] == null ).should.be.true();
		( endpoints["api/v1/event/keyed/error/limit/25/offset"] == null ).should.be.true();

		// due to collections defined in local shape:
		endpoints["api/v1/event/keyed/info/limit/25/offset/0"].count.should.be.Number().which.is.equal( 28 );
		endpoints["api/v1/event/keyed/info/limit/25/offset/0"].items.should.be.Array().which.has.length( 25 );
		endpoints["api/v1/event/keyed/info/limit/25/offset/0"].items.map( r => r.index ).join( "," ).should.be.equal( "5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29" );
		endpoints["api/v1/event/keyed/info/limit/25/offset/25"].count.should.be.Number().which.is.equal( 28 );
		endpoints["api/v1/event/keyed/info/limit/25/offset/25"].items.should.be.Array().which.has.length( 3 );
		endpoints["api/v1/event/keyed/info/limit/25/offset/25"].items.map( r => r.index ).join( "," ).should.be.equal( "30,31,32" );

		endpoints["api/v1/event/keyed/error/limit/25/offset/0"].count.should.be.Number().which.is.equal( 17 );
		endpoints["api/v1/event/keyed/error/limit/25/offset/0"].items.should.be.Array().which.has.length( 17 );
		endpoints["api/v1/event/keyed/error/limit/25/offset/0"].items.map( r => r.index ).join( "," ).should.be.equal( "1,2,3,4,33,34,35,36,37,38,39,40,41,42,43,44,45" );
	} );
} );


function data( shapeOverlay ) {
	const localShape = shape();

	if ( shapeOverlay ) {
		merge( localShape, shapeOverlay );
	}

	return qualifyFakedRecords( {
		e1: { type: "error", index: 1 },
		e2: { type: "error", index: 2 },
		e3: { type: "error", index: 3 },
		e4: { type: "error", index: 4 },
		e5: { type: "info", index: 5 },
		e6: { type: "info", index: 6 },
		e7: { type: "info", index: 7 },
		e8: { type: "info", index: 8 },
		e9: { type: "info", index: 9 },
		e10: { type: "info", index: 10 },
		e11: { type: "info", index: 11 },
		e12: { type: "info", index: 12 },
		e13: { type: "info", index: 13 },
		e14: { type: "info", index: 14 },
		e15: { type: "info", index: 15 },
		e16: { type: "info", index: 16 },
		e17: { type: "info", index: 17 },
		e18: { type: "info", index: 18 },
		e19: { type: "info", index: 19 },
		e20: { type: "info", index: 20 },
		e21: { type: "info", index: 21 },
		e22: { type: "info", index: 22 },
		e23: { type: "info", index: 23 },
		e24: { type: "info", index: 24 },
		e25: { type: "info", index: 25 },
		e26: { type: "info", index: 26 },
		e27: { type: "info", index: 27 },
		e28: { type: "info", index: 28 },
		e29: { type: "info", index: 29 },
		e30: { type: "info", index: 30 },
		e31: { type: "info", index: 31 },
		e32: { type: "info", index: 32 },
		e33: { type: "error", index: 33 },
		e34: { type: "error", index: 34 },
		e35: { type: "error", index: 35 },
		e36: { type: "error", index: 36 },
		e37: { type: "error", index: 37 },
		e38: { type: "error", index: 38 },
		e39: { type: "error", index: 39 },
		e40: { type: "error", index: 40 },
		e41: { type: "error", index: 41 },
		e42: { type: "error", index: 42 },
		e43: { type: "error", index: 43 },
		e44: { type: "error", index: 44 },
		e45: { type: "error", index: 45 },
	}, localShape );
}

function shape() {
	return fakeModelShape( "event", {}, {
		properties: {
			$model: "'event'",
			$id: "item( $segments, length( $segments ) - 1 )",
			type: "type",
			index: "index",
		},
		collections: {
			"{limit:10}": {},
			"limit/20/offset/{limit:20}": {},
			"keyed/{key}/{limit:10}": {
				key: "type",
			},
			"keyed/{key}/limit/25/offset/{limit:25}": {
				key: "type",
			},
		},
	} );
}
