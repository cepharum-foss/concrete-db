import { describe, it } from "mocha";
import "should";

import { cure } from "../../lib/index.mjs";
import { FakeCollector, FakeGenerator, fakeModelShape } from "./helper.mjs";


describe( "Curing concrete-db", () => {
	it( "does not generate any database endpoint when no records have been collected", async() => {
		const generator = new FakeGenerator();
		const collector = new FakeCollector( {} );

		await cure( [], {}, { collector, generator } );

		generator.writtenDatabase.endpoints.should.be.deepEqual( {} );
	} );

	it( "generates database endpoint when records have been collected", async() => {
		const shape = fakeModelShape( "employee", {
			properties: {
				$id: "id || item( $segments, length( $segments ) - 1 )",
				surname: "surname",
				firstname: "firstname || givenname",
				label: "label || test( surname && ( firstname || givenname ), surname + ', ' + ( firstname || givenname ), null )",
				age: "age",
			},
			defaults: {
				age: 34
			},
			collections: {
				"": {
					key: true,
					filter: true,
					properties: {
						label: "label",
						age: "age",
					},
				},
				"index/age": {
					key: "age",
					properties: {
						label: "label",
					},
					reduce: true,
				},
				"index/surname": {
					key: "surname",
					properties: {
						label: "firstname",
					},
					reduce: true,
				},
			},
		} );

		const generator = new FakeGenerator();
		const collector = new FakeCollector( {
			"some/path/john-doe": [
				{
					shape,
					folder: "$fake",
					segments: [ "some", "path", "john-doe" ],
					data: {
						model: "employee",
						FirstName: "John",
						surname: "Doe",
					},
				}
			],
			"some/path/name2": [
				{
					shape,
					folder: "$fake",
					segments: [ "some", "path", "name2" ],
					data: {
						model: "employee",
						id: "jane-doe",
						surname: "Doe",
						givenName: "Jane",
						LABEL: "Jane Doe",
						age: 38,
					},
				}
			],
		}, shape );

		await cure( [], {}, { collector, generator } );

		generator.writtenDatabase.endpoints.should.be.deepEqual( {
			"api/v1/employee/item/john-doe": {
				firstname: "John",
				surname: "Doe",
				label: "Doe, John",
				age: 34
			},
			"api/v1/employee/item/jane-doe": {
				firstname: "Jane",
				surname: "Doe",
				label: "Jane Doe",
				age: 38,
			},
			"api/v1/employee": {
				count: 2,
				items: [
					{
						$id: "john-doe",
						label: "Doe, John",
						age: 34,
					},
					{
						$id: "jane-doe",
						label: "Jane Doe",
						age: 38,
					},
				],
			},
			"api/v1/employee/index/surname": {
				Doe: {
					count: 2,
					items: [
						{
							$id: "john-doe",
							label: "John",
						},
						{
							$id: "jane-doe",
							label: "Jane",
						},
					],
				},
			},
			"api/v1/employee/index/age": {
				34: {
					count: 1,
					items: [
						{
							$id: "john-doe",
							label: "Doe, John",
						},
					],
				},
				38: {
					count: 1,
					items: [
						{
							$id: "jane-doe",
							label: "Jane Doe",
						},
					],
				},
			},
		} );
	} );
} );
