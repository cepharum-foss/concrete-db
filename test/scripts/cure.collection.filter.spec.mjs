import { describe, it } from "mocha";
import "should";

import { cure } from "../../lib/index.mjs";
import { FakeCollector, FakeGenerator, fakeModelShape, qualifyFakedRecords } from "./helper.mjs";


describe( "Curing: creating collection with filtered list of items", () => {
	it( "generates regular collection endpoints for all items passing filter defined per model", async() => {
		const generator = new FakeGenerator();
		const source = data();
		const collector = new FakeCollector( source, source.$shape );

		await cure( [], {}, { collector, generator } );

		generator.writtenDatabase.endpoints["api/v1/friend"].should.be.deepEqual( {
			count: 1,
			items: [{
				$id: "max",
				surname: "Smith",
				firstname: "Maxine",
			}],
		} );

		generator.writtenDatabase.endpoints["api/v1/adversary"].should.be.deepEqual( {
			count: 2,
			items: [ {
				$id: "paula",
				surname: "Miller",
				firstname: "Paula",
			}, {
				$id: "jason",
				surname: "Lee",
				firstname: "Jason",
			} ],
		} );
	} );

	it( "generates keyed collection endpoints for all items passing filter defined per model", async() => {
		const generator = new FakeGenerator();
		const source = data();
		const collector = new FakeCollector( source, source.$shape );

		await cure( [], {}, { collector, generator } );

		generator.writtenDatabase.endpoints["api/v1/friend/keyed"].should.be.deepEqual( {
			Smith: {
				count: 1,
				items: [{
					$id: "max",
					surname: "Smith",
					firstname: "Maxine",
				}],
			},
		} );

		generator.writtenDatabase.endpoints["api/v1/adversary/keyed"].should.be.deepEqual( {
			Miller: {
				count: 1,
				items: [{
					$id: "paula",
					surname: "Miller",
					firstname: "Paula",
				}],
			},
			Lee: {
				count: 1,
				items: [{
					$id: "jason",
					surname: "Lee",
					firstname: "Jason",
				}],
			},
		} );
	} );
} );


function data() {
	return qualifyFakedRecords( {
		"data/john": { model: "friend", id: "john", surname: "Doe", firstname: "John" },
		"data/max": { model: "friend", id: "max", surname: "Smith", firstname: "Maxine" },
		"data/paula": { model: "adversary", id: "paula", surname: "Miller", firstname: "Paula" },
		"data/jason": { model: "adversary", id: "jason", surname: "Lee", firstname: "Jason" },
	}, shape() );
}

function shape() {
	return fakeModelShape( "friend", {
		collections: {
			"": {
				filter: "length( surname ) > 4",
			},
			keyed: {
				filter: "length( surname ) > 4",
				key: "surname",
				reduce: true,
			},
		},
	}, {
		properties: {
			$model: "model || item( $segments, length( $segments ) - 2 )",
			$id: "id || item( $segments, length( $segments ) - 1 )",
			surname: "surname",
			firstname: "firstname",
		},
		collections: {
			keyed: {
				key: "surname",
				reduce: true,
			},
		},
	} );
}
