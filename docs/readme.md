---
home: true
heroImage: /logo.svg
heroText: null
tagline: Provide data for ages.
actionText: Getting started
actionLink: /tutorial/getting-started.html
features:
- title: High performance
  details: All available data is computed in advance. Requesting it takes as little as fetching a static HTML document.
- title: Scalable
  details: There are no server-side sessions. Thus, load-balancing your data served from multiple hosts is a no-brainer.
- title: Secure
  details: Lacking support for change of data at runtime, there is no authentication, too, thus nothing to hack.
- title: Ecological
  details: Saves energy at runtime of your application by abolishing powerful server-hardware and database engines.
- title: Sustainable
  details: Build your data to be used for ages. The reduced software stack is easier to maintain long term.
- title: Efficient
  details: It perfectly integrates with CI/CD and reorganizes human-readable YAML files for optimum use by application. 
footer: MIT Licensed | Copyright © 2022 cepharum GmbH
---

* [About our motivation](about.md)
* [Glossary](glossary.md)
* [Curing your data](about/curing.md)
