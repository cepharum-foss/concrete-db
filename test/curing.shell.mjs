import { resolve, dirname } from "path";
import File from "fs";
import { execSync } from "child_process";
import { fileURLToPath } from "url";

import FileEssentials from "file-essentials";


const here = fileURLToPath( dirname( import.meta.url ) );
const d = name => resolve( here, name );

( async() => {
	await FileEssentials.rmdir( d( "output" ) );
	await FileEssentials.rmdir( d( "my-data" ) );

	await FileEssentials.mkdir( here, "my-data" );

	await File.promises.writeFile( d( "my-data/.concrete-db.shape.yaml" ), `
models:
  poi:
    properties:
      lat: lat || latitude
      lng: lng || long || longitude
      label: label || title
      description: description || text
      tags: slugify( asarray( type || tags ) )
`.trim(), "utf8" );

	await File.promises.writeFile( d( "my-data/bg.record.yaml" ), `
MODEL: poi
id: brandenburg-gate
Lat: 52.51628
Long: 13.37771
Label: Brandenburger Tor
type:
  - monument
  - Landmark
  - building
  - public
`.trim(), "utf8" );

	await File.promises.writeFile( d( "my-data/tv-tower.record.yaml" ), `
Model: poi
ID: berlin-tv-tower
Latitude: 52.52082
Longitude: 13.40942
title: TV Tower
type: LANDMARK
`.trim(), "utf8" );


	execSync( `npm run cure -- -v -o ${d( "output" )} ${d( "my-data" )}` );
} )();
