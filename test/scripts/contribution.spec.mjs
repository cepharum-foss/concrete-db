import { describe, it } from "mocha";
import merge from "lodash.merge";
import "should";

import { cure } from "../../lib/index.mjs";

import {
	FakeCollector,
	FakeGenerator,
	qualifyFakedRecords
} from "./helper.mjs";
import { DefaultShape } from "../../lib/defaults.mjs";

describe( "Input records of one model contributing to another model", () => {
	it( "can be declared in shape but doesn't generate anything without contributing input", async() => {
		const { collector, generator } = setup( {} );

		await cure( [], {}, { collector, generator } );

		generator.writtenDatabase.should.be.deepEqual( {
			prefix: "api/v1/",
			endpoints: {},
		} );
	} );

	it( "can be declared in shape and feeds another model that doesn't have any records itself", async() => {
		const { collector, generator } = setup( {
			"work/123": {
				author: "John Doe",
				title: "How to contribute.",
			},
			"work/567": {
				author: "John Doe",
				title: "How to contribute even more.",
			},
		} );

		await cure( [], {}, { collector, generator } );

		generator.writtenDatabase.should.be.deepEqual( {
			prefix: "api/v1/",
			endpoints: {
				"api/v1/work": {
					count: 2,
					items: [
						{
							$id: "123",
							author: "John Doe",
							title: "How to contribute.",
						},
						{
							$id: "567",
							author: "John Doe",
							title: "How to contribute even more.",
						},
					],
				},
				"api/v1/work/item/123": {
					author: "John Doe",
					title: "How to contribute.",
				},
				"api/v1/work/item/567": {
					author: "John Doe",
					title: "How to contribute even more.",
				},
				"api/v1/author": {
					count: 1,
					items: [
						{
							$id: "john-doe",
							name: "John Doe",
							numWorks: 2,
							works: [
								"123", "567"
							]
						}
					]
				},
				"api/v1/author/item/john-doe": {
					name: "John Doe",
					numWorks: 2,
					works: [ "123", "567" ],
				},
				"api/v1/search/index/lead": {
					c: {
						count: 1
					},
					d: {
						count: 1
					},
					e: {
						count: 1
					},
					h: {
						count: 1
					},
					j: {
						count: 1
					},
					m: {
						count: 1
					},
				},
				"api/v1/search/index/lead/c": {
					count: 1,
					items: [
						{
							$id: "contribute",
							lead: "c"
						}
					]
				},
				"api/v1/search/index/lead/d": {
					count: 1,
					items: [
						{
							$id: "doe",
							lead: "d"
						}
					]
				},
				"api/v1/search/index/lead/e": {
					count: 1,
					items: [
						{
							$id: "even",
							lead: "e"
						}
					]
				},
				"api/v1/search/index/lead/h": {
					count: 1,
					items: [
						{
							$id: "how",
							lead: "h"
						}
					]
				},
				"api/v1/search/index/lead/j": {
					count: 1,
					items: [
						{
							$id: "john",
							lead: "j"
						}
					]
				},
				"api/v1/search/index/lead/m": {
					count: 1,
					items: [
						{
							$id: "more",
							lead: "m"
						}
					]
				},
				"api/v1/search/item/contribute": {
					refs: [
						{
							id: "123",
							model: "work"
						},
						{
							id: "567",
							model: "work"
						}
					]
				},
				"api/v1/search/item/doe": {
					refs: [
						{
							id: "123",
							model: "work"
						},
						{
							id: "567",
							model: "work"
						}
					]
				},
				"api/v1/search/item/even": {
					refs: [
						{
							id: "567",
							model: "work"
						}
					]
				},
				"api/v1/search/item/how": {
					refs: [
						{
							id: "123",
							model: "work"
						},
						{
							id: "567",
							model: "work"
						}
					]
				},
				"api/v1/search/item/john": {
					refs: [
						{
							id: "123",
							model: "work"
						},
						{
							id: "567",
							model: "work"
						}
					]
				},
				"api/v1/search/item/more": {
					refs: [
						{
							id: "567",
							model: "work"
						}
					]
				}
			},
		} );
	} );

	it( "can contribute to single item each", async() => {
		const { collector, generator } = setup( {
			"foo/123": {
				data: "John Doe",
			},
			"foo/567": {
				data: "Jane Doe",
			},
		}, {
			models: {
				foo: {
					contributions: {
						bar: {
							$id: "data",
							refs: "data"
						}
					}
				}
			}
		} );

		await cure( [], {}, { collector, generator } );

		generator.writtenDatabase.should.be.deepEqual( {
			prefix: "api/v1/",
			endpoints: {
				"api/v1/foo/item/123": {
					data: "John Doe",
				},
				"api/v1/foo/item/567": {
					data: "Jane Doe",
				},
				"api/v1/bar/item/John Doe": {
					refs: "John Doe",
				},
				"api/v1/bar/item/Jane Doe": {
					refs: "Jane Doe",
				},
			},
		} );
	} );

	it( "can contribute to multiple items each", async() => {
		const { collector, generator } = setup( {
			"foo/123": {
				data: [ 1, 2 ],
			},
			"foo/567": {
				data: [ 4, 5 ],
			},
		}, {
			models: {
				foo: {
					contributions: {
						bar: {
							$id: "data",
							refs: "data"
						}
					}
				}
			}
		} );

		await cure( [], {}, { collector, generator } );

		generator.writtenDatabase.should.be.deepEqual( {
			prefix: "api/v1/",
			endpoints: {
				"api/v1/foo/item/123": {
					data: [ 1, 2 ],
				},
				"api/v1/foo/item/567": {
					data: [ 4, 5 ],
				},
				"api/v1/bar/item/1": {
					refs: [ 1, 2 ],
				},
				"api/v1/bar/item/2": {
					refs: [ 1, 2 ],
				},
				"api/v1/bar/item/4": {
					refs: [ 4, 5 ],
				},
				"api/v1/bar/item/5": {
					refs: [ 4, 5 ],
				},
			},
		} );
	} );

	it( "can contribute to same item each in replacement mode", async() => {
		const { collector, generator } = setup( {
			"foo/123": {
				data: 1,
			},
			"foo/567": {
				data: 2,
			},
		}, {
			models: {
				foo: {
					contributions: {
						bar: {
							$id: "1",
							refs: "data"
						}
					}
				}
			}
		} );

		await cure( [], {}, { collector, generator } );

		generator.writtenDatabase.should.be.deepEqual( {
			prefix: "api/v1/",
			endpoints: {
				"api/v1/foo/item/123": {
					data: 1,
				},
				"api/v1/foo/item/567": {
					data: 2,
				},
				"api/v1/bar/item/1": {
					refs: 2,
				},
			},
		} );
	} );

	it( "can contribute to same item each in collecting mode", async() => {
		const { collector, generator } = setup( {
			"foo/123": {
				data: 1,
			},
			"foo/567": {
				data: 2,
			},
		}, {
			models: {
				foo: {
					contributions: {
						bar: {
							$id: "1",
							"refs[]": "data"
						}
					}
				}
			}
		} );

		await cure( [], {}, { collector, generator } );

		generator.writtenDatabase.should.be.deepEqual( {
			prefix: "api/v1/",
			endpoints: {
				"api/v1/foo/item/123": {
					data: 1,
				},
				"api/v1/foo/item/567": {
					data: 2,
				},
				"api/v1/bar/item/1": {
					refs: [ 1, 2 ],
				},
			},
		} );
	} );

	it( "can contribute to same item each in distributed mode", async() => {
		const { collector, generator } = setup( {
			"foo/123": {
				data: 1,
			},
			"foo/567": {
				data: 2,
			},
		}, {
			models: {
				foo: {
					contributions: {
						bar: {
							$id: "1",
							"refs...": "data"
						}
					}
				}
			}
		} );

		await cure( [], {}, { collector, generator } );

		generator.writtenDatabase.should.be.deepEqual( {
			prefix: "api/v1/",
			endpoints: {
				"api/v1/foo/item/123": {
					data: 1,
				},
				"api/v1/foo/item/567": {
					data: 2,
				},
				"api/v1/bar/item/1": {
					refs: 2,
				},
			},
		} );
	} );

	it( "can contribute to same item each in combined collecting/distributed mode", async() => {
		const { collector, generator } = setup( {
			"foo/123": {
				data: 1,
			},
			"foo/567": {
				data: 2,
			},
		}, {
			models: {
				foo: {
					contributions: {
						bar: {
							$id: "1",
							"refs[]...": "data"
						}
					}
				}
			}
		} );

		await cure( [], {}, { collector, generator } );

		generator.writtenDatabase.should.be.deepEqual( {
			prefix: "api/v1/",
			endpoints: {
				"api/v1/foo/item/123": {
					data: 1,
				},
				"api/v1/foo/item/567": {
					data: 2,
				},
				"api/v1/bar/item/1": {
					refs: [ 1, 2 ],
				},
			},
		} );
	} );

	it( "can contribute to same list of items each in replacement mode", async() => {
		const { collector, generator } = setup( {
			"foo/123": {
				data: 1,
			},
			"foo/567": {
				data: 2,
			},
		}, {
			models: {
				foo: {
					contributions: {
						bar: {
							$id: "array( 3, 4 )",
							refs: "data"
						}
					}
				}
			}
		} );

		await cure( [], {}, { collector, generator } );

		generator.writtenDatabase.should.be.deepEqual( {
			prefix: "api/v1/",
			endpoints: {
				"api/v1/foo/item/123": {
					data: 1,
				},
				"api/v1/foo/item/567": {
					data: 2,
				},
				"api/v1/bar/item/3": {
					refs: 2,
				},
				"api/v1/bar/item/4": {
					refs: 2,
				},
			},
		} );
	} );

	it( "can contribute to same list of items each in collecting mode", async() => {
		const { collector, generator } = setup( {
			"foo/123": {
				data: 1,
			},
			"foo/567": {
				data: 2,
			},
		}, {
			models: {
				foo: {
					contributions: {
						bar: {
							$id: "array( 3, 4 )",
							"refs[]": "data"
						}
					}
				}
			}
		} );

		await cure( [], {}, { collector, generator } );

		generator.writtenDatabase.should.be.deepEqual( {
			prefix: "api/v1/",
			endpoints: {
				"api/v1/foo/item/123": {
					data: 1,
				},
				"api/v1/foo/item/567": {
					data: 2,
				},
				"api/v1/bar/item/3": {
					refs: [ 1, 2 ],
				},
				"api/v1/bar/item/4": {
					refs: [ 1, 2 ],
				},
			},
		} );
	} );

	it( "can contribute to same list of items each in distributed mode", async() => {
		const { collector, generator } = setup( {
			"foo/123": {
				data: [ 12, 14 ],
			},
			"foo/567": {
				data: [ 16, 18 ],
			},
		}, {
			models: {
				foo: {
					contributions: {
						bar: {
							$id: "array( 3, 4 )",
							"refs...": "data"
						}
					}
				}
			}
		} );

		await cure( [], {}, { collector, generator } );

		generator.writtenDatabase.should.be.deepEqual( {
			prefix: "api/v1/",
			endpoints: {
				"api/v1/foo/item/123": {
					data: [ 12, 14 ],
				},
				"api/v1/foo/item/567": {
					data: [ 16, 18 ],
				},
				"api/v1/bar/item/3": {
					refs: 16,
				},
				"api/v1/bar/item/4": {
					refs: 18,
				},
			},
		} );
	} );

	it( "can contribute to same list of items each in combined collecting/distributed mode", async() => {
		const { collector, generator } = setup( {
			"foo/123": {
				data: [ 12, 14 ],
			},
			"foo/567": {
				data: [ 16, 18 ],
			},
		}, {
			models: {
				foo: {
					contributions: {
						bar: {
							$id: "array( 3, 4 )",
							"refs[]...": "data"
						}
					}
				}
			}
		} );

		await cure( [], {}, { collector, generator } );

		generator.writtenDatabase.should.be.deepEqual( {
			prefix: "api/v1/",
			endpoints: {
				"api/v1/foo/item/123": {
					data: [ 12, 14 ],
				},
				"api/v1/foo/item/567": {
					data: [ 16, 18 ],
				},
				"api/v1/bar/item/3": {
					refs: [ 12, 16 ],
				},
				"api/v1/bar/item/4": {
					refs: [ 14, 18 ],
				},
			},
		} );
	} );
} );


function setup( recordsByPathname, shapeOverlay ) {
	const source = qualifyFakedRecords( recordsByPathname, merge( {}, DefaultShape, shape( shapeOverlay ) ) );
	const collector = new FakeCollector( source, source.$shape );
	const generator = new FakeGenerator();

	return { collector, generator };
}

function shape( overlay ) {
	return merge( {
		common: {
			properties: {
				$model: "item( $segments, length( $segments ) - 2 )",
				$id: "item( $segments, length( $segments ) - 1 )",
			}
		},
		models: {
			work: {
				properties: {
					author: "author",
					title: "title",
				},
				contributions: {
					author: {
						$id: "slugify(author)",
						name: "author",
						"works[]": "$id",
					},
					search: {
						$id: "keys( spread( array( author, title ), 5, 3, 32, true ) )",
						"refs[]": "dict('model', 'work', 'id', $id)",
					},
				},
			},
			author: {
				properties: {
					name: "name",
					works: "works",
					numWorks: "length(works)",
				},
			},
			search: {
				properties: {
					refs: "test( $post, refs, array() )",
					// test() isn't required, but demonstrated before ... could be as simple as this:
					// refs: "refs",
				},
				collections: {
					"": false,
					"index/lead": {
						key: "substring($id,0,1)",
						properties: {
							lead: "substring($id,0,1)"
						},
					}
				}
			},
			foo: {
				properties: {
					data: "data"
				},
				collections: {
					"": false,
				},
			},
			bar: {
				properties: {
					refs: "refs"
				},
				collections: {
					"": false,
				},
			},
		},
	}, overlay || {} );
}
