# Routes

Every route of generated database consists of these parts:

* a common prefix defined in curing options,
* a model's name,
* a custom route addressing a collection or some managed route addressing a single item and
* the file extension `.json`.

## Prefix

The prefix is provided in runtime options for customizing the curing process. It defaults `api/v1`.

## Model's name

On converting records into items one first step is to associate either record with a model by extracting the model's name. This name is appended to the prefix for all routes addressing data of that model.

:::tip Example
While processing a collected record it is detected to belong to model named `mymodel`. This name is also used in all routes regarding items and collections of that model.
:::

## Managed routes

For every item of the model, another endpoint is created exposing either item. The related route per item is the combination of `item/` with the item's ID as discovered right after detecting its model.

:::tip Example
After collecting record detected to represent item of model `mymodel` with ID `12345`, the resulting endpoint's route is `item/12345`. This, in turn, becomes

```
api/v1/mymodel/item/12345
```
:::

## Custom routes

Shapes define zero or more collection endpoints per model to be generated in addition to those item-exposing endpoints. Their route must not start with `item/`, but apart from that any name is accepted including the empty string.

:::tip Example
On model `mymodel` the custom route `index/by-name` results in

```
api/v1/mymodel/index/by-name
```

Using empty string as custom route results in

```
api/v1/mymodel
```
:::

:::tip Note!
Provide the custom route without any leading slash.
:::

## File extension

When generating files and folders for every route addressing an endpoint, the extension `.json` is appended to assure that the resulting file is delivered with proper content type by a static web server with default configuration.

:::tip Example
Previously compiled custom route

```
api/v1/mymodel/index/by-name
```

results in file written at

```
api/v1/mymodel/index/by-name.json
```
:::

## Common limitations

Routes are accepted unless one of its segments

* is `.`, 
* is `..` or 
* ends with `.json` (except for the last segment).
