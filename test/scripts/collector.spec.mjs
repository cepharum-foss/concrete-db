import { resolve } from "path";
import { fileURLToPath } from "url";

import { describe, it } from "mocha";
import "should";

import { Collector } from "../../lib/index.mjs";


const DataFolder = resolve( fileURLToPath( import.meta.url ), "../../data" );

describe( "Collecting records from a folder", () => {
	describe( "returns empty set when invoked", () => {
		it( "without any argument", async() => {
			const collector = new Collector();

			await collector.fromFolders();

			collector.records.size.should.be.equal( 0 );
		} );

		it( "without list of folders", async() => {
			const collector = new Collector();

			await collector.fromFolders( [] );

			collector.records.size.should.be.equal( 0 );
		} );

		it( "with list of folders that don't contain any record file", async() => {
			const collector = new Collector();

			await collector.fromFolders( [resolve( DataFolder, "empty" )] );

			collector.records.size.should.be.equal( 0 );
		} );
	} );

	describe( "returns non-empty set when invoked", () => {
		it( "with list of folders that contain at least one record file", async() => {
			const collector = new Collector();

			await collector.fromFolders( [resolve( DataFolder, "map" )] );

			collector.records.size.should.be.greaterThanOrEqual( 1 );
		} );
	} );
} );
