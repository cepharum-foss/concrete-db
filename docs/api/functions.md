# Additional term functions

This is a list of function available on evaluating terms in ConcreteDB. This list applies in addition to [those included with the terms processor](https://cepharum-foss.gitlab.io/simple-terms/functions.html).

## String comparison

### compare()

**Signature:** `compare( left, right )`

Compares string values of `left` and `right` case-sensitively and locale-aware. 

Returns

* `-1` if `left < right`,
* `1` if `left > right` or
* `0` if strings are equal.

### icompare()

**Signature:** `icompare( left, right )`

Compares string values of `left` and `right` case-insensitively and locale-aware. 

Returns

* `-1` if `left < right`,
* `1` if `left > right` or
* `0` if strings are equal.

## Data management

### foreignkey()

**Signature:** `foreignkey( model, ID )`

Creates and returns reference addressing item selected by its `ID` of a model selected by its `name`.

When casting returned reference to string, it is reduced to combination of model's name and item's ID separated from each other by single forward slash.

:::tip Example
A reference created with `foreignkey( "friend", "john" )` is represented as string like this:

```
friend/john
```
:::

### resolve()

**Signature:** `resolve( models, reference )`

Fetches item addressed by `reference` created with `foreignkey()` from pool of items per model provided in `models`. It is returning found item or nullish value if item is missing.

:::warning Context-Specific Function
You must provide a pool of items per model yourself. That's why this function is basically useful [on creating collections](/about/terms.md#term-context-on-generating-collections) by using `$database` available there, only.
:::

## Statistical functions

### spread() <Badge text="v0.2.0+"></Badge>

**Signature:** `spread( input, minSize, minTermSize, maxTermSize, ignoreCase, strict, wordPattern )`

_All arguments but first one are optional. Defaults have been omitted here to improve readability. See description below!_

The function counts occurrences of words in provided input and returns the result as a [dictionary](https://cepharum-foss.gitlab.io/simple-terms/functions.html#dict).

A _word_ is any consecutive sequence of letters, digits, underscores and dashes, though dashes aren't accepted in leading position. For example, the term

```javascript
spread( " This is John Doe!!!" )
```

yields a dictionary equivalent to the result of term

```javascript
dict( "john", 1, "doe", 1, "this", 1 )
```

which is describing data resembling

```JSON
{ "john": 1, "doe": 1, "this": 1 }
```

in JSON. The word `is` hasn't been extracted due to default limits applying. [See below.](#minsize)

Extraction accepts complex input data which is processed recursively. The term

```javascript
spread( array( "John Doe", true, "Jane Doe" ) )
```

yields

```javascript
dict( "john", 1, "jane", 1, "doe", 2 )
```

This first argument is always required. All other arguments are optional and have defaults when omitted:

#### minSize

This option controls the minimum number of characters required in a string to actually consider it for extracting any potential word from it. This option is meant to prevent keywords or custom boolean literals like `"yes"` or `"no"` to be extracted as words.

The default is 10.

```javascript
spread( array( "yes", "no", "on", "off", "default", "undefined" ) )
```

would yield an empty dictionary for neither provided value is reaching minimum size of 10 characters.

#### minWordSize

This option is similar to the previous one, but controls the minimum size of a potential word to consider. 

The default is 3.

```javascript
spread( "Uh, he is on duty." )
```

would yield single-item dictionary

```javascript
dict( "duty", 1 )
```

for all other words are too short. Usually, this option is meant to prevent huge lists of matches for very short words that happen to occur rather often.

#### maxWordSize

This is the counterpart to [minWordSize](#minwordsize) as it controls the maximum size of a found word to consider. Picking up very complex words may bloat the resulting database mostly without benefit for people might not be interested in rare complex words.

However, there is no limit by default.

#### ignoreCase

This boolean option controls whether any two words only differing in case of letters are considered the same word or not.

The default is `true`.

#### strict

This boolean option requests to ignore any scalar data which isn't a string. When set,

```javascript
spread( true )
```

yields an empty dictionary for `true` isn't a string but a boolean. However, if `strict` isn't set, the boolean is converted to string first and inspected as such afterwards, thus the same term would yield dictionary including word `"true"`.

The default is `true`.

:::tip Prepare for fulltext search
This function combined with support for [contributions](../about/curing.md#processing-contributions) is an essential element for supporting client-side fulltext search with ConcreteDB.
:::

#### wordPattern <Badge text="v0.2.1+"></Badge>

This string option provides the source of a global Unicode-aware regular expression to use for matching a word instead of the default which is

```regexp
[\p{L}\p{N}_][\p{L}\p{N}_-]*
```

The regular expression is provided as string, thus backslashes need to be escaped by another backslash. The resulting regular expression is configured as global and Unicode-aware.

For example, if you want words to consist of letters and digits, only, you need to provide this string:

```javascript
"[\\p{L}\\p{N}]+"
```

## Process control

### fail()

**Signature:** `fail( message )`

Generates an error with message provided in first argument.

On processing shapes, either term may include checks for testing resulting data's validity. In case some found data isn't suitable for generating output, this function can be used to intentionally create an error which is causing the curing process to fail.

```yaml
models:
  work:
    properties:
      author: first( author, fail( "missing author" ) )
      title: first( title, fail( "missing title" ) )
```

This example illustrates a shape that's computing properties `author` and `title` based on a provided record. If either author or title are falsy, the error generated by `fail()` gets delivered as value of property which is causing curing process to stop on error.
