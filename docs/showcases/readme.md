# Showcases

## DFG Project "Berliner Repertorium"

[DFG project "Berliner Repertorium" of Humboldt-Universität zu Berlin](https://repertorium.sprachen.hu-berlin.de/) (German only)

This is an archived yet fully functional online database providing research data collected over years on Latin hymns and their medieval translations into German language. It is implemented with [VuePress](https://vuepress.vuejs.org/) featuring serverless searching, browsing scans of antique books and converting presented data into PDF.

Its model is divided into hymns, their translations into German, documents witnessing those translations, manuscripts and prints containing those witnessing documents. There are 471 [hymns](https://repertorium.sprachen.hu-berlin.de/browse/hymn.html), 3.066 translations, 7.427 witnessing documents, 849 manuscripts and 23 prints.

On curing, [additional models are derived from those records](../about/curing.md#processing-contributions) for presenting indices into the data. This includes 139 [clerical feasts](https://repertorium.sprachen.hu-berlin.de/browse/feast.html), 69 [authors](https://repertorium.sprachen.hu-berlin.de/browse/author.html) and 128 libraries and institutions in 108 places owning those [manuscripts](https://repertorium.sprachen.hu-berlin.de/browse/manuscript.html) and [prints](https://repertorium.sprachen.hu-berlin.de/browse/printing.html). For implementing the [serverless search](https://repertorium.sprachen.hu-berlin.de/browse/search.html), 43.806 words are extracted from either model's records.

As a result, about 11.800 YAML documents with 1.2 MB of data are converted into 56.300 JSON documents with a total size of 59 MB which is 50 times the amount of original data. The curing process takes up to a few minutes with most time spent on writing resulting files.
