declare namespace ConcreteDB {
	export interface CuringOptions extends CollectorOptions, ShaperOptions, GeneratorOptions {
		/**
		 * Selects base folder for writing files of resulting concrete-db.
		 */
		outputFolder?: string;

		/**
		 * Lists custom functions to expose in simple terms used in shape
		 * processing. For security reasons, you cannot replace regular term
		 * functions this way by intention.
		 */
		library?: { [functionName: string]: () => any };
	}

	export interface CollectorOptions {
		pattern?: RegExp;
		nameToKey?: (name: string) => string;
	}

	export interface ShaperOptions {
		/**
		 * Picks common prefix for all created routes. Defaults to `api/v1`.
		 */
		prefix?: string;
	}

	export interface GeneratorOptions {
		/**
		 * Indicates if extra files in output folder should be removed.
		 */
		clean?: boolean;
	}

	/**
	 * Implements curing stage of collecting source records.
	 */
	export class Collector {
		/**
		 * Maps keys of found YAML files into list of either file's content.
		 *
		 * Keys are derived from either file's path name relative to the
		 * containing base folder as provided to `fromFolders()`.
		 */
		records: Map<string, CollectedRecord[]>

		/**
		 * Exposes local shapes of processed folders in order of processing.
		 */
		localShapes: Shape[];

		/**
		 * Collects sources from provided folders.
		 *
		 * @param folders path names of folders to search for supported YAML files
		 */
		fromFolders( folders: string[] ): Promise<Collector>;
	}

	/**
	 * Implements curing stage of transforming source records and creating
	 * endpoints of resulting database.
	 */
	export class Shaper {
		/**
		 * Converts provided set of collected records into a set of endpoints
		 * with associated data to expose.
		 *
		 * @param records
		 * @param localShapes local shapes of folders records have been collected from in same order as folders processed
		 */
		transform( records: CollectedSources, localShapes: Shape[] ): Promise<Database>;
	}

	/**
	 * Implements curing stage of writing endpoints of database e.g. to
	 * file system.
	 */
	export class Generator {
		/**
		 * Creates files or similar persistent containers for either endpoint of
		 * provided database to expose the related data.
		 *
		 * @param database
		 */
		writeDatabase( database: Database ): Promise<Generator>;
	}

	/**
	 * Aggregates custom implementations to use on curing database.
	 */
	export interface CuringStages {
		collector?: Collector;
		shaper?: Shaper;
		generator?: Generator;
	}

	export type DatabaseItem = object;

	export interface DatabaseRegularCollection {
		count?: number;
		items: Array<DatabaseItem>
	}

	export type DatabaseKeyedCollection = {
		[key in string]: DatabaseRegularCollection;
	};

	export type DatabaseCollection = (DatabaseRegularCollection | DatabaseKeyedCollection);
	export type DatabaseEndpointData = (DatabaseCollection | DatabaseItem);

	export interface DatabaseEndpoints {
		[path: string]: DatabaseEndpointData;
	}

	export interface Database {
		prefix: string;
		endpoints: DatabaseEndpoints;
	}

	export interface CollectedSources {
		[id: string]: CollectedRecord[];
	}

	export interface CollectedRecord {
		folder: string;
		segments: string[];
		data: object;
		shape: Shape;
	}

	/**
	 * Describes a model's item processed during shaping stage.
	 */
	export interface ModelItem {
		/**
		 * Lists segments of relative path of file this item's originating
		 * record has been read from.
		 */
		$segments: string[];

		/**
		 * Refers to record this item is based on.
		 */
		$original: CollectedRecord;

		/**
		 * Exposes shape of item's model in context of its originating record.
		 */
		$shape: ShapeModel;

		/**
		 * Names this item's model.
		 */
		$model: string;

		/**
		 * Uniquely identifies the item in context of its model.
		 */
		$id: string;

		/**
		 * item's properties
		 */
		[name: string]: any;
	}

	/**
	 * Lists default values for properties of a single model's item.
	 */
	export interface ShapePropertyDefaults {
		[target: string]: any;
	}

	/**
	 * Lists rules fetched to compile a record of information (e.g. single item
	 * of model or an such an item's representation in a collection).
	 */
	export interface ShapeProperties {
		/**
		 * Provides term computed to get the name of a record's model.
		 */
		$model?: string;

		/**
		 * Provides term computed to get ID of item described by a record.
		 */
		$id?: string;

		/**
		 * Provides term computed to get ID of selected item's variant described
		 * by record.
		 */
		$variant?: string;

		[propertyName: string]: any;
	}

	export enum ShapeCollectionType {
		INDEX = "index",
	}

	export type ShapeSorterTerm = string;

	export interface ShapeSorterSelector {
		property: string;
		ascending?: boolean;
	}

	/**
	 * Describe configuration of a single collection to generate for a model.
	 */
	export interface ShapeCollectionInstance {
		key?: string;
		filter?: string;
		properties?: ShapeProperties;
		sort?: (ShapeSorterSelector | ShapeSorterTerm);
	}

	/**
	 * Describe configuration of an existing collection associated with another
	 * endpoint.
	 */
	export interface ShapeCollectionReference {
		reference?: string;
	}

	export type ShapeCollection = ( ShapeCollectionInstance | ShapeCollectionReference );

	/**
	 * Describes collections to generate for a model.
	 */
	export type ShapeCollections = ShapeCollection[];

	/**
	 * Defines optional constraints to apply.
	 */
	export interface ShapeConstraints {
		/**
		 * If set, multiple source records addressing same target record aren't
		 * merged, but some exception is thrown.
		 *
		 * This doesn't affect definition of variants. However, merging single
		 * variant from multiple sources is rejected, too.
		 */
		singleSource?: boolean;
	}

	/**
	 * Describes rules controlling shaping of a model.
	 */
	export interface ShapeModel {
		defaults: ShapePropertyDefaults;
		properties?: ShapeProperties;
		collections?: ShapeCollections;
		contributions?: {
			[contributedModelName: string]: {
				[targetPropertyName: string]: string;
			};
		};

		constraints?: ShapeConstraints;
	}

	export interface Shape {
		/**
		 * Controls whether this shape is merged with a more generic one or not.
		 */
		root?: boolean;

		/**
		 * Provides common shaping for models to use unless there are more
		 * specific rules for a particular model.
		 */
		common: ShapeModel;

		/**
		 * Provides specific shaping rules per named model.
		 */
		models: {
			[name: string]: ShapeModel;
		}
	}
}
