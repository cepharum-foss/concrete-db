import { describe, it } from "mocha";
import merge from "lodash.merge";
import "should";

import { cure } from "../../lib/index.mjs";

import {
	FakeCollector,
	FakeGenerator,
	qualifyFakedRecords
} from "./helper.mjs";
import { DefaultShape } from "../../lib/defaults.mjs";

describe( "A model's shape including some validity checks to require property set", () => {
	it( "does not prevent valid data from being cured", async() => {
		const { collector, generator } = setup( {
			"work/123": {
				author: "John Doe",
				title: "How to contribute.",
			},
			"work/567": {
				author: "John Doe",
				title: "How to contribute even more.",
			},
		} );

		await cure( [], {}, { collector, generator } ).should.be.resolved();

		generator.writtenDatabase.should.be.deepEqual( {
			prefix: "api/v1/",
			endpoints: {
				"api/v1/work/item/123": {
					author: "John Doe",
					title: "How to contribute.",
				},
				"api/v1/work/item/567": {
					author: "John Doe",
					title: "How to contribute even more.",
				},
			},
		} );
	} );

	it( "fails curing as soon as check fails", async() => {
		const { collector, generator } = setup( {
			"work/123": {
				title: "How to contribute.",
			},
			"work/567": {
				author: "John Doe",
				title: "How to contribute even more.",
			},
		} );

		await cure( [], {}, { collector, generator } ).should.be.rejectedWith( /missing author/ );
	} );
} );


function setup( recordsByPathname ) {
	const source = qualifyFakedRecords( recordsByPathname, merge( {}, DefaultShape, shape() ) );
	const collector = new FakeCollector( source, source.$shape );
	const generator = new FakeGenerator();

	return { collector, generator };
}

function shape() {
	return {
		common: {
			properties: {
				$model: "uncollect( slice( $segments, -2 ) )",
				$id: "uncollect( slice( $segments, -1 ) )",
			},
			collections: {
				"": false,
			},
		},
		models: {
			work: {
				properties: {
					author: "first( author, fail( 'missing author' ) )",
					title: "title",
				}
			},
		},
	};
}
