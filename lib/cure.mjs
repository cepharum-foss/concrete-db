import { Collector } from "./collector.mjs";
import { Shaper } from "./shaper.mjs";
import { Generator } from "./generator.mjs";

/**
 * Converts record files in selected folders into database.
 *
 * @param {string|string[]} folders path name(s) of one or more folders to search for YAML-formatted record files to transform
 * @param {CuringOptions} options options for customizing conversion process
 * @param {CuringStages} stages provides custom handlers per curing stage to use instead of default ones
 * @returns {Promise<void>} promises successful conversion of found records
 */
export async function cure( folders, options = {}, stages = {} ) {
	const sources = Array.isArray( folders ) ? folders : folders ? [folders] : [];
	const _stages = {
		collector: stages?.collector || new Collector( options ),
		shaper: stages?.shaper || new Shaper( options ),
		generator: stages?.generator || new Generator( options ),
	};

	if ( !( _stages.collector instanceof Collector ) ) {
		throw new TypeError( "invalid type of collector" );
	}

	if ( !( _stages.shaper instanceof Shaper ) ) {
		throw new TypeError( "invalid type of shaper" );
	}

	if ( !( _stages.generator instanceof Generator ) ) {
		throw new TypeError( "invalid type of generator" );
	}

	const collector = await _stages.collector.fromFolders( sources );
	const database = await _stages.shaper.transform( collector.records, collector.localShapes );
	await _stages.generator.writeDatabase( database );
}
