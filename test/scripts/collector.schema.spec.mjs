import { resolve } from "path";
import { fileURLToPath } from "url";

import { describe, it } from "mocha";
import "should";

import { Collector } from "../../lib/index.mjs";
import { DefaultShape } from "../../lib/defaults.mjs";


const DataFolder = resolve( fileURLToPath( import.meta.url ), "../../data" );

describe( "Collector reading records from a folder", () => {
	it( "attaches default shape if there is no local shape at top level of provided folder", async() => {
		const collector = new Collector();

		await collector.fromFolders( [resolve( DataFolder, "shape/no-local" )] );

		collector.records.should.have.size( 4 );
		for ( const records of collector.records.values() ) {
			records.should.be.an.Array().which.has.length( 1 );

			for ( const record of records ) {
				record.shape.should.be.deepEqual( DefaultShape );
				record.data.title.should.match( /^item\d+$/ );
			}
		}
	} );

	it( "attaches local shape as overlay atop default shape if there is one at top level of provided folder", async() => {
		const collector = new Collector();

		await collector.fromFolders( [resolve( DataFolder, "shape/local" )] );

		collector.records.should.have.size( 4 );
		for ( const records of collector.records.values() ) {
			records.should.be.an.Array().which.has.length( 1 );

			for ( const record of records ) {
				record.shape.should.not.be.deepEqual( DefaultShape );
				record.shape.models.should.have.size( 2 ).and.have.properties( [ "foo", "bar" ] );
				record.shape.models.foo.should.deepEqual( record.shape.models.bar );
				record.data.title.should.match( /^item\d+$/ );
			}
		}
	} );

	it( "ignores local shapes that are located in a sub-folder of folder provided for reading records from", async() => {
		const collector = new Collector();

		await collector.fromFolders( [resolve( DataFolder, "shape/sub-only" )] );

		collector.records.should.have.size( 4 );
		for ( const records of collector.records.values() ) {
			records.should.be.an.Array().which.has.length( 1 );

			for ( const record of records ) {
				record.shape.should.be.deepEqual( DefaultShape );
				record.data.title.should.match( /^item\d+$/ );
			}
		}
	} );
} );
