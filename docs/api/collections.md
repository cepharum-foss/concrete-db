# Collection definition

[Collections](/glossary.md#collection) are defined in [model shapes](/api/shapes.md#model-shape). This document is providing details on how to define a collections. In addition, extended features are explained.

## Types of collections

### Regular collections

_Regular collections_ are sequential lists of items combined with the total number of matching items.

  ```json
  {
    "count": numberOfItems,
    "items": [ item1, item2, ... ]
  }
  ```

They can be optionally [sorted](#sort) and [sliced](#slicing).

:::tip Total number
The provided count of items is always representing the _total number_ of matching items. 

By default, it's identical to the number of actually listed items. But on [slicing](#slicing), a subset of items can be presented per regular collection endpoint, thus total number might be different from number of listed items.
:::

### Keyed collections

_Keyed collections_ are mapping distinct values of a key property into [regular collections](#regular-collections) listing items matching either key value. They resemble grouped queries in a relational database. 

  :::tip Example
  Keyed collections are useful to implement filtered lists of items. The user can pick a filter value from a list and gets all the items containing that selected value. 
  :::

  There is base endpoint exposing available key values and either one's number of matching items:

  ```json
  {
    "key1": {
      "count": numberOfItemsWithKey1
    },
    "key2": {
      "count": numberOfItemsWithKey2
    },
    ...
  }
  ```
  
  For every key value like `key1` and `key2` in this example, another endpoint is exposed to list either one's matching items as a [regular collection](#regular-collections):

  ```json
  {
    "count": numberOfItemsWithKey1,
    "items": [ item1, item2, item6, ... ]
  }
  ```

  ```json
  {
    "count": numberOfItemsWithKey2,
    "items": [ item4, item6, item9, ... ]
  }
  ```

  This separation into multiple endpoints has been introduced in **v0.2.0** to improve support for larger data sets and reduce unnecessary load on network in those cases. Previously, all endpoints have been merged into a single endpoint which is still [available on demand](#reduce):

  ```json
  {
    "key1": {
      "count": numberOfItemsWithKey1,
      "items": [ item1, item2, item6, ... ]
    },
    "key2": {
      "count": numberOfItemsWithKey2,
      "items": [ item4, item6, item9, ... ]
    },
    ...
  }
  ```

## Definition syntax

Creation of a collection is controlled by a map of rules. These are the rules available:

### filter

This is a string containing a [term](/about/terms.md) evaluated in context of an item. It is assumed to retrieve truthy value on items that should be included with the collection and a falsy value otherwise.

When omitted, every item is included by default.

:::tip Example
```yaml
collections:
  list/blocked:
    filter: blocked
```

This rule will create a collection listing all items with value of property `blocked` being truthy in full detail.
:::

:::warning Different Term Context
The `filter` term is evaluated in a [different context](/about/terms.md#term-context-on-generating-collections) than terms evaluated for deriving items.
:::

### key

The definition of a key triggers creation of a keyed collection. It is a term evaluated in context of either item which has passed the filter for calculating that item's key value.

:::tip Example
```yaml
collections:
  index/by-state:
    key: blocked
```

This rule creates a collection grouping all items by their value of property `blocked`.
:::

:::warning Different Term Context
The `key` term is evaluated in a [different context](/about/terms.md#term-context-on-generating-collections) than terms evaluated for deriving items.
:::

### properties

Every item in a regular collection is represented by all its properties, by default. 

```yaml
collections:
  "": {}
```

You can limit the number of exposed properties or even use computed ones derived from actual properties by defining a map of property names into terms deriving their value in resulting collection.

```yaml
collections:
  "":
    properties:
      name: name
      active: !blocked
```

This works similar to [defining an item's properties](/api/shapes.md#properties) on [deriving it from a record](/about/curing.md#compiling-model-items). Major differences are the [context of information](/about/terms.md#term-context-on-generating-collections) available to computed terms and the way [keywords in terms are resolved](/about/terms.md#case-sensitive-data).

### reduce <Badge text="v0.2.0+"></Badge>

By default, a keyed collection results in multiple endpoints generated:

* One endpoint is exposed under configured custom route listing all available key values and the related number of items matching either key.

  ```json
  { 
    "foo": { "count": 3 }, 
    "bar": { "count": 1 }, 
    "baz": { "count": 6 }
  }
  ```

* For every key value, another endpoint is created for listing the items matching that key value, e.g. the endpoint for key value `foo` could deliver data like this:

  ```json
  { "count": 3, "items": [
    { "$id": 6, "label": "The sixth foo!" },  
    { "$id": 9, "label": "The ninth foo!" },  
    { "$id": 8, "label": "The eighth foo!" },  
  ] }
  ```

On setting boolean option `reduce` in a keyed collection's definition, the previous behavior of exposing all data in a single endpoint is selected:

```json
{ 
  "foo": { "count": 3, "items": [
	{ "$id": 6, "label": "The sixth foo!" },
	{ "$id": 9, "label": "The ninth foo!" },
	{ "$id": 8, "label": "The eighth foo!" },
  ] },
  "bar": { "count": 1, "items": [ 
	... 
  ] }, 
  "baz": { "count": 6, "items": [ 
	... 
  ] }
}
```

:::warning Performance impact
Reducing keyed collections can affect your application's response time as **all data** must be fetched e.g. prior to offering a list of filter options the user could pick from to limit data retrieval.

**Don't use `reduce` option on larger data sets because of that.**
:::

### reference

:::danger Deprecated <Badge type="error" text="v0.2.0+"></Badge>
This feature is deprecated and is planned for future removal.

We suggest using [node anchors and references](https://en.wikipedia.org/wiki/YAML#Advanced_components) as supported by YAML instead.
:::

See the [separate section on re-using existing collections](#re-using-collections), below.

### sort

Entries of a [regular collection](#types-of-collections) can be sorted in two different ways using this rule:

* A map picks a property of every entry in collection to sort by. 

* A string provides a term evaluated repeatedly in a special context to compare two candidates of collection's entries.

#### Entries of collection vs. items of model

This section is referring to entries of a collection by intention to explicitly distinguish those from items of the related model. This is important for entries are representing items in collection and may use a different set of properties for that. 

#### Sorting by collection property

When providing a map, it must contain `property` naming the property of [collection's entries](#entries-of-collection-vs-items-of-model) to sort by.

:::warning Just to be sure ...
The selected property [must be exposed in the collection](#properties).
:::

```yaml
collections:
  "":
    sort:
      property: name
```

This will sort all entries of a regular collection by values of either entry's property `name` in ascending order.

In addition, boolean `descending` may be set true for switching to descending order of sorting.

```yaml
collections:
  "desc":
    sort:
      property: name
      descending: true
```

Sorting by property handles numbers and other values differently. When sorting in ascending order:

* entries with numeric value in selected property are inserted at the beginning of resulting list,
* entries with nullish value in selected property are inserted at the end of resulting list,
* numeric values are sorted numerically,
* all other non-nullish values are cast to string and sorted using [`localeCompare()`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/String/localeCompare).

#### Sorting by evaluating term

By providing a string, it is assumed to contain a [term](/about/terms.md) for repeated evaluation in [a special context](/about/terms.md#term-context-on-sorting-regular-collections): On every evaluation, two [entries of collection](#entries-of-collection-vs-items-of-model) are picked for comparison and exposed as `$left` and `$right`. The term is meant to return

* values less than 0, if `$left` < `$right`,
* values greater than 0, if `$left` > `$right` or
* 0 if both values are equivalent.

```yaml
collections:
  by/age:
    sort: $right.age - $left.age || $right.yearsEmployed - $left.yearsEmployed
```

:::warning Different Term Context
Terms are evaluated in a [special context](/about/terms.md#term-context-on-sorting-regular-collections).
:::

## Extended features

### Keyed routes

By default, all [endpoints](../glossary.md#endpoint) of a [keyed collection](#types-of-collections) are exposed at [routes](/about/routes.md) based on a fixed pattern. For example, let's assume a collection has been configured like this:

```yaml
collections:
  by/department:
    key: department
```

* A base endpoint is created due to that configuration. Its route is 

  `/api/v1/foo/by/department.json` 

  and it is exposing distinct values of items' property `department` together with number of items matching either value.

  ```http request
  GET /api/v1/foo/by/department.json HTTP/1.0
  
  {
    "home": {
      "count": numberOfHomeDepartments,
    },
    "emea": {
      "count": numberOfEmeaDepartments,
    }
  }
  ```

* Another endpoint is created for every value listed before and its route is generated by appending that value as another segment: 

  `/api/v1/foo/by/department/home.json`  
  `/api/v1/foo/by/department/emea.json`

  ```http request
  GET /api/v1/foo/by/department/home.json HTTP/1.0
  
  {
    "count": numberOfHomeDepartments,
    "items": [ ... items of home department ... ],
  }
  ```

  ```http request
  GET /api/v1/foo/by/department/emea.json HTTP/1.0
  
  {
    "count": numberOfEmeaDepartments,
    "items": [ ... items of EMEA department ... ]
  }
  ```

In configuration, _keyed routes_ per collection are available to replace that fixed pattern with a custom one:

```yaml
collections:
  "by/department/{key}/data":
    key: department
```

The marker `{key}` used in route of this example is switching collection generation to keyed routes. Instead of concatenating the route with either key value, the given string is computed per value and the placeholder `{key}` is replaced with either value.

```http request
GET /api/v1/foo/by/department/home/data.json HTTP/1.0

{
	"count": numberOfHomeDepartments,
	"items": [ ... items of home department ... ],
}
```

```http request
GET /api/v1/foo/by/department/emea/data.json HTTP/1.0

{
	"count": numberOfEmeaDepartments,
	"items": [ ... items of EMEA department ... ]
}
```

:::warning There is no overview!
This result looks identical to the fixed pattern at first glance. But there is a significant difference: collections with keyed routes omit that base endpoint listing available key values as an overview.

That is due to the freedom of declaring an arbitrary pattern for generating routes which does not cover routes omitting values representing by that placeholder. This becomes rather obvious when using keyed routes to slice keyed collections.
:::

### Slicing

By default, regular collections always include every matching item. When used with larger sets of data, fetching huge lists of items is affecting a client's latency and generating mostly unnecessary load on the network.

```yaml
collections:
  "": {}
```

```http request
GET /api/v1/foo.json HTTP/1.0

{
	"count": 5000,
	"items": [ .... 5000 items ... ]
}
```

For any regular collection its definition can enable _slicing_ for mitigating that issue by using sole occurrence of `{limit:n}` with `n` replaced with a positive integer in the associated route:

```yaml
collections:
  "limit/10/offset/{limit:10}": {}
```

This will create separate endpoints _instead of_ a single one:

```http request
GET /api/v1/foo/limit/10/offset/0.json HTTP/1.0

{
	"count": 5000,
	"items": [ .... first chunk of 10 items ... ]
}
```

```http request
GET /api/v1/foo/limit/10/offset/10.json HTTP/1.0

{
	"count": 5000,
	"items": [ .... second chunk of 10 items ... ]
}
```

```http request
GET /api/v1/foo/limit/10/offset/20.json HTTP/1.0

{
	"count": 5000,
	"items": [ .... third chunk of 10 items ... ]
}
```

... and so on ... 

Every such endpoint is delivering the total number of available items as illustrated.

#### Slicing keyed collections

Slicing isn't available for [keyed collections](#types-of-collections) unless using [keyed routes](#keyed-routes). That's because of the fixed pattern of routes applied to endpoints of a keyed collection otherwise.


### Re-using collections

:::danger Deprecated <Badge type="error" text="v0.2.0+"></Badge>
This feature is deprecated and is planned for future removal.

We suggest using [node anchors and references](https://en.wikipedia.org/wiki/YAML#Advanced_components) as supported by YAML instead.
:::

In ConcreteDB there are no actual endpoints processing parameters for even slightly adjusting any delivered set of items at runtime. If you want to enable your client to fetch a list of items with different sorting order applied, you need to define separate collections:

```yaml
collections:
  "":
    sort:
      property: name
  "desc":
    sort:
      property: name
      descending: true
```

Properly embedded in a shape, this could be a working example: first collection is listing all items sorted by `name` in ascending order, the second one is providing the same list of items, but this time sorted by `name` in descending order. A client could fetch the first collection with `GET /api/v1/foo.json` and the second one with `GET /api/v1/foo/desc.json`.

The problem is: both collections are created independently of each other. Therefore, the curing is extracting the same list of items each with the same set of calculated properties just to apply different sortings. This redundant processing is significantly affecting runtime of curing process in case of defining lots of similar collections and/or working with larger sets of data.

For mitigating this issue, collections may refer to another collection defined in same model shape for re-using its result of extracting items.

```yaml
collections:
  "":
    sort:
      property: name
  "desc":
    reference: ""
    sort:
      property: name
      descending: true
```

The `reference` rule must replace some optionally used `filter`, `key` or `properties` rule for they work mutually exclusively. That's why the following example **does not work:**

```yaml
collections:
  "":
    sort:
      property: name
  "desc":
    reference: ""
    key: age
    sort:
      property: name
      descending: true
```

Referencing collections is mostly designed to help with using different sortings as well as extended features described here.
