import File from "fs";
import Path from "path";
import EventEmitter from "events";

import PromiseEssentials from "promise-essentials";
import FileEssentials from "file-essentials";
import YAML from "yaml";
import merge from "lodash.merge";

import {
	ShapeFileName,
	DefaultCollectorOptions,
	DefaultShape
} from "./defaults.mjs";
import { deepFreeze } from "./helper.mjs";
import { YAMLParserOptions } from "./options.mjs";


/**
 * Implements class collecting record files.
 *
 * @property {CuringOptions} options customizations for collecting source records
 * @property {CollectedSources} records named records of data collected from YAML files
 */
export class Collector extends EventEmitter {
	/**
	 * @param {CuringOptions} options customizations for collecting
	 */
	constructor( options = null ) {
		super();

		Object.defineProperties( this, {
			options: {
				value: { ...DefaultCollectorOptions, ...options },
				enumerable: true
			},
			records: { value: new Map(), enumerable: true },
			localShapes: { value: [], enumerable: true },
		} );
	}

	/**
	 * Fetches shape for selected folder.
	 *
	 * @param {string} folder path name of a folder
	 * @returns {Promise<object>} promises shape of selected folder
	 */
	async getLocalShape( folder ) {
		const localFilename = Path.resolve( folder, ShapeFileName );
		let fileContent;

		try {
			fileContent = await File.promises.readFile( localFilename, { encoding: "utf8" } );
		} catch ( cause ) {
			if ( cause.code !== "ENOENT" ) {
				throw cause;
			}
		}

		const shape = fileContent ? YAML.parse( fileContent, YAMLParserOptions ) : undefined;
		if ( shape && shape.root ) {
			return deepFreeze( merge( {}, DefaultShape, shape ) );
		}

		return deepFreeze( merge( {}, DefaultShape, this.options.shape, shape ) );
	}

	/**
	 * Collects records from YAML files found in provided folders. The resulting
	 * hierarchy of records follows hierarchy of processed folders.
	 *
	 * @param {string[]} folders lists folders to process
	 * @returns {Promise<Collector>} promises collector having collected records from folders
	 */
	async fromFolders( folders ) {
		const spinner = "⠁⠂⠄⡀⢀⠠⠐⠈";
		const { pattern, nameToKey } = this.options;

		await PromiseEssentials.each( folders || [], async( folder, index ) => {
			if ( this.options.verbose ) {
				process.stderr.write( `collecting records from folder ${folder} [${index + 1}/${folders.length}] ... ` );
			}

			const localShape = await this.getLocalShape( folder );
			let total = 0;
			let additional = 0;

			this.localShapes.push( localShape );

			await FileEssentials.find( folder, {
				filter: ( localPath, fullPath, stat ) => {
					const filename = Path.basename( fullPath );

					if ( filename.startsWith( "." ) ) {
						return false;
					}

					if ( stat.isDirectory() ) {
						// required for descending into sub-folders
						return true;
					}

					return stat.isFile() && pattern.test( filename );
				},
				converter: async( localPath, fullPath, stat ) => {
					if ( stat.isFile() ) {
						const content = await File.promises.readFile( fullPath, { encoding: "utf8" } );
						const name = nameToKey( localPath );
						const record = {
							folder,
							segments: name.split( Path.sep ),
							data: YAML.parse( content, YAMLParserOptions ),
							shape: localShape,
						};

						if ( this.records.has( name ) ) {
							this.records.get( name ).push( record );
							additional++;
						} else {
							this.records.set( name, [record] );
						}

						total++;

						if ( this.options.verbose ) {
							process.stderr.write( `${spinner[Math.floor( total / 10 ) % spinner.length]}\x08` );
						}
					}

					return undefined;
				},
				waitForConverter: true,
			} );

			if ( this.options.verbose ) {
				process.stderr.write( `${total} records (${additional} overlays)\n` );
			}
		} );

		return this;
	}
}
