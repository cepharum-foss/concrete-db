import { ForeignKey } from "./helper.mjs";

/**
 * Extracts last element of provided list.
 *
 * @param {array} list list of values
 * @returns {any|null} last value of list, null if not a list or empty
 */
export function last( list ) {
	return Array.isArray( list ) && list.length > 0 ? list[list.length - 1] : null;
}

/**
 * Creates special value representing reference to uniquely identified item of
 * selected model.
 *
 * @param {string} model name of referenced item's model
 * @param {string|number} id unique identifier of referenced item
 * @returns {ForeignKey} representation of desired reference
 */
export function foreignkey( model, id ) {
	return new ForeignKey( model, id );
}

/**
 * Fetches item selected by foreign key from provided pool of items per model.
 *
 * @param {DatabaseCollection} models pool of items per model
 * @param {ForeignKey} reference addresses item to fetch
 * @returns {DatabaseItem} fetched item or nullish value on missing item
 */
export function resolve( models, reference ) {
	return models && reference instanceof ForeignKey ? reference.pickFrom( models ) : undefined;
}

/**
 * Compares two strings case-insensitively.
 *
 * @param {string} left left operand of comparison
 * @param {string} right right operand of comparison
 * @returns {number} -1 if left is less than right, 1 if right is less than left, 0 if both strings are equal
 */
export function icompare( left, right ) {
	return String( left ).toLocaleLowerCase().localeCompare( String( right ).toLocaleLowerCase() );
}

/**
 * Compares two strings case-sensitively.
 *
 * @param {string} left left operand of comparison
 * @param {string} right right operand of comparison
 * @returns {number} -1 if left is less than right, 1 if right is less than left, 0 if both strings are equal
 */
export function compare( left, right ) {
	return String( left ).localeCompare( String( right ) );
}

/**
 * Extracts list of words from provided scalar or non-scalar value.
 *
 * A word is a consecutive sequence of letters, digits, underscores and dashes
 * though dashes are rejected in a leading position. On setting custom option
 * for ignoring case, all letters of collected words are converted to lowercase
 * in resulting map.
 *
 * @param {any} value value to extract words from
 * @param {number} minSize minimum number of characters in a string to be considered for extracting words
 * @param {number} minWordSize minimum number of characters in extracted words
 * @param {number} maxWordSize maximum number of characters in extracted words
 * @param {boolean} ignoreCase set true to drop case of extracted words
 * @param {boolean} strict set false to have any non-string value converted to string instead of being ignored
 * @param {string} wordPattern provides source of global unicode regexp matching a word to generate
 * @returns {Object<string,number>} map of extracted words into either words's number of occurrences
 */
export function spread( value, minSize = 10, minWordSize = 3, maxWordSize = Infinity, ignoreCase = true, strict = true, wordPattern = undefined ) {
	const result = {};

	for ( const word of generateWords( value, minSize, ignoreCase, strict, wordPattern ) ) {
		if ( word.length >= minWordSize && word.length <= maxWordSize ) {
			result[word] = ( result[word] || 0 ) + 1;
		}
	}

	return result;
}

/**
 * Generates error with provided message for failing curing process.
 *
 * @param {string} message description of error
 * @returns {Error} generated error
 */
export function fail( message ) {
	return new Error( message );
}


/**
 * Recursively iterates over provided source converting all scalar data into
 * string prior to yielding consecutive sequences of letters and digits.
 *
 * @param {any} source data to extract terms from
 * @param {number} minSize minimum number of characters in a string to be considered for extracting terms
 * @param {boolean} ignoreCase set true to drop case of extracted terms
 * @param {boolean} strict set false to have any non-string value converted to string instead of being ignored
 * @param {string} wordPattern provides source of global unicode regexp matching a word to generate
 * @returns {Generator<string|*, void, *>} iterator over flat sequence of terms
 */
function *generateWords( source, minSize, ignoreCase, strict, wordPattern = undefined ) {
	let stream;

	if ( Array.isArray( source ) ) {
		stream = source;
	} else if ( source instanceof Set || source instanceof Map ) {
		stream = source.values();
	} else if ( source && typeof source === "object" ) {
		stream = Object.values( source );
	} else {
		if ( strict && typeof source !== "string" ) {
			return;
		}

		const string = String( source ).trim();

		if ( string.length < minSize ) {
			return;
		}

		const ptn = wordPattern ? new RegExp( wordPattern, "gu" ) : /[\p{L}\p{N}_][\p{L}\p{N}_-]*/gu;
		let match;

		while ( ( match = ptn.exec( string ) ) ) {
			yield ignoreCase ? ( match[1] ?? match[0] ).toLocaleLowerCase() : match[1] ?? match[0];
		}

		return;
	}

	for ( const sub of stream ) {
		yield* generateWords( sub, minSize, ignoreCase, strict, wordPattern );
	}
}
