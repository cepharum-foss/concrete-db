# About

ConcreteDB is a read-only database providing small to medium size data sets over REST API with high performance and horizontal scalability.

## Motivation

### Status quo: websites

Websites often rely on an online content management system (CMS) which is relying on some sophisticated database engine and on server-side scripting. However, these tools are either idling most of the time unless there are visitors to the website. Or they keep generating the same content in response to the same requests for the presented content does not change that often.

Static site generators became popular lately e.g. to mitigate this waste of processing power.

### Status quo: rich web applications

Let's transfer this problem to rich web applications. Most requests sent to a backend are retrieving data which does not change that often, either. Nonetheless, applications usually work with a single full-feature database engine as backend rather than integrating separate engines each handling parts of the issue for improved overall efficiency.

Just think of a small-business web shop: presented products don't change each and every day. However, every page view is probably causing the same queries against some database engine re-collecting the same information over and over again. Only a subset of requests is actually changing data, e.g. when processing orders. Even on caching data queries, a scripting engine is used to translate those cached queries and embed resulting data in rather static views to be delivered to the client eventually. 

Such dynamic data could be managed by separate services, though. Orders could be put in a much smaller database. They could be delivered to an existing ERP system. Or they could be forwarded to you by mail via some gateway. Comments and satisfaction surveys can be implemented via some social network. After extracting all dynamic features, the static part of your application could be implemented with static site generators as well. However, those tools often lack proper support for common features provided by an actual database backend such as searching, slicing and sorting collections of items.

### Suggestion

ConcreteDB is designed to fill this gap. It comes with a script named `cure` converting folders with YAML files into another folder with JSON files laid out to resemble the REST API of a database engine. This way, rich web applications can access data via REST API while actually reading some static files, only.

```mermaid
graph LR
	s1[folder w/ YAML files]
	s2[folder w/ YAML files]
	s3[folder w/ YAML files]
	c{{collector}}
	s{{shaper}}
	g{{generator}}
	d[folder w/ JSON files]
	s1-->c
	s2-->c
	s3-->c
	c-->s
	s-->g
	g-->d
	style c fill:#c30045,stroke:#575757,color:white
	style s fill:#c30045,stroke:#575757,color:white
	style g fill:#c30045,stroke:#575757,color:white
```

We call this conversion process the [_curing_](/about/curing.md) of the database:

1. YAML files are recursively _collected_ from one or more folders.

2. Their content is _shaped_ according to [shape definitions](about/shapes.md).

3. JSON files are _generated_ from the resulting database.

## Preferred setup

A ConcreteDB is generated from several YAML-formatted files each describing an [item](/glossary.md#item) of resulting [database](/glossary.md#database).

:::tip What about JSON sources?
The YAML parser included with ConcreteDB is basically capable of processing JSON, too. That's because every JSON document is also a valid YAML document.
:::

This generation can be integrated with any tool. We suggest running it as part of continuous delivery/deployment supported by several modern online project management tools like [GitLab](https://gitlab.com/) which is establishing lots of further benefits such as versioning of data sources, their collaborative management and the provision of resulting databases as container images using private or public container registries.

## Pros

### Performance

ConcreteDB instances perform great. In fact, they don't require any particular server-side engine. All it takes is a web server as simple as [nginx](https://nginx.org/) delivering static files from a folder. This results in highest performance possible for supported requests.

### Horizontal scalability

ConcreteDB instances scale well horizontally. It is because of using read-only data in files. No matter if these files reside on one server or multiple servers e.g. used behind some load balancer. All requests deliver the same information as long as all involved servers contain the same version of your database.

### Security

ConcreteDB instances are quite secure. There is no risk of being vulnerable to SQL injections or similar. There is no chance of exploiting implementation flaws in the database engine at runtime, for ConcreteDB is working in a remote background: all source of provided data and the script for curing a database instance can reside anywhere you like, even in a private network behind a highly restrictive firewall pushing resulting files to the public web server _from_ there. This limits opportunities for an attacker of your public site to permanently compromise your data. In addition, any failed or corrupted server can be rebuilt from scratch in no time by simply re-pushing a previous build of your cured ConcreteDB instance.
  
### Portability

ConcreteDB instances are portable. Every piece of information is exposed. Accessing it is as simple as opening a file or - if there is a proper web server exposing those files - sending a REST-compliant request to the server. All data is JSON-encoded, which is an open and well-established format, either.

### Long-term support

Think of an archive you've created and that you want to keep running for as many years as possible without ever taking care of it again. ConcreteDB is a perfect match in such a case, too, for it does not have any runtime that needs to be maintained long term. There are no security updates someone has to install. There are no breaking changes you have to deal with e.g. by transforming your data or by changing the way your application is accessing it. That won't ever happen.

## Cons

### Read-only data

A ConcreteDB instance is read-only. That's by design. There is no support for adjusting its data at runtime by any means. However, even in a rich web application working with a backend server most requests are about fetching information and quite few requests are changing any data. Thus, on mixing ConcreteDB for mostly static information to be fetched with a small database engine, load on the latter can be massively reduced.

### Redundancy

A ConcreteDB is highly redundant due to exposing some piece of information in many files in different ways, thus consuming quite some amount of storage space on a drive. This is by design, too. ConcreteDB isn't meant to replace existing databases in each and every use case. Huge sets of data should not be managed in a ConcreteDB. But the vast amount of websites and rich web applications rely on a rather small set of data. ConcreteDB is designed to replace rather static parts of those applications.

### Limited interactions

Interactions with a ConcreteDB instance are pretty limited and must be planned ahead of time. This is due to its design, as well. For there is no engine processing requests at runtime, all requests are limited to the filesystem hierarchy generated before while curing the database. 
  
Of course, you could run a smart proxy adding further interactions to an existing ConcreteDB instance without recreating the database files. But without that, you need to declare indices and views on your data prior to curing the database and this might result in increased redundancy, too. You could reconsider your client application's design as well for sure.

### Limited client support

ConcreteDB does not have any console or GUI for querying data and for visually presenting it in tables or charts. It is designed to work with rich client applications, only. You need to have a proxy re-processing any data otherwise. However, most websites turn into rich client applications nowadays.

### Common exposure

When exposing the resulting ConcreteDB instance, all its data is accessible in the same way. You might prevent parts of data from being listed in indices. You could also create endpoints with individual or even random hashes used in endpoints of resulting REST API to disguise parts of the data. However, as soon as someone knows the proper URL of any data available, that data is retrievable unconditionally.
  
If that is an issue, you should consider using a properly configured reverse proxy such as [Caddy](https://caddyserver.com/) for adding authenticated sessions and controlling access on selected endpoints after authentication, only.
