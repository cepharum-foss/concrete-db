# Shape syntax

All [shapes](../about/shapes.md) are hierarchical maps. They provide rules processed on [curing](../readme.md) a ConcreteDB.

When read from files, shapes are YAML-encoded. Thus, examples in this document use YAML encoding.

## Top-level properties

### root

This boolean switch indicates, whether some shape is a root shape or not. Default is `false`.

When processing record files from multiple base folders, every such base folder may include its own shape definition file named `.concrete-db.shape.yaml`. Those files are merged with the global shape unless this property is set `true`. In that case, the read file is replacing global shape on processing its base folder.

:::tip Merging  
Shapes are deeply merged. Thus, you can have partial [local shapes](/about/shapes.md#local-shapes) replacing only some elements of underlying [global shape](/about/shapes.md#global-shape).  
:::

### common

This is a [model shape](#model-shape) providing a common set of rules for processing any model's records on [curing](/glossary.md#curing) a [database](/glossary.md#database). It is used as a base for merging with a [model-specific set of rules](#models).

### models

This section maps names of models into partial [model shapes](#model-shape) used to qualify [common rules](#common) for processing either named model.

## Model shape

On [curing](/glossary.md#curing) a [database](/glossary.md#database), for every collected [record](/glossary.md#record) the name of its [model](/glossary.md#model) is detected first. After that, a _model shape_ is created by deeply merging the [set of common rules](#common) with a [custom set of rules for that model](#models). It is used to process either record as well as to create [collections](#collections) per model.

### defaults

This map is listing default values for an item's properties. It is mapping a property's name into the related value.

```yaml
defaults:
  name: unnamed item
  priority: 5
  blocked: false
```

You should provide a default value unless it is _nullish_, only. The normative set of a model's properties per item [is declared separately](#properties). Defaults for properties missing in that declaration are ignored.

:::warning Meta Properties
Property names starting with `$` are reserved for so called [meta properties](#meta-properties). You cannot provide default values for those.
:::

#### Optional scripting support

Default values are processed as given. See the example above for illustration.

You may use [simple terms](/about/terms.md) to derive information, though. To do so, provide a default value as a literal string starting with single assignment operator followed by the term to evaluate in context of processed record:

```yaml
defaults:
  name: =join(array('unnamed', "item"), " ")
  priority: =(6 * 2) - 7
  blocked: =!true
```

This second example results in identical default values as the first one above.

### properties

This map is naming all properties of an [item](/glossary.md#item). Every name is associated with a [term](/about/terms.md) to be computed in context of a [record](/glossary.md#record) for calculating either property's value.

:::warning Meta Properties
Support for names starting with `$` is limited. These are reserved for [meta properties](#meta-properties) and just some of them are customizable here.
:::

#### Mandatory scripting support

Unlike default values, declaring all properties of a resulting item strongly depends on the originally collected record's values. Thus, all entries in this map are associating a property's name with a term unconditionally. Therefore, you don't need to use an assignment operator to start a term.

```yaml
properties:
  name: name
  priority: prio || priority
  blocked: first( blocked, locked, disabled )
  role: test( first( admin, superuser ), "admin", "user" )
```

:::warning Prevent Fallbacks
Terms in this map should provide values actually found in originating record, only. Do not use literal fallbacks here, but [use separately defined defaults](#defaults).

**BAD:**
```yaml
properties:
  name: name || "unnamed item"
```

**GOOD:**
```yaml
defaults:
  name: unnamed item
properties:
  name: name
```

This becomes essential when processing multiple records for the same resulting item. 

Just consider having two collected record files found in separate source folders with one declaring a base record and the other declaring a variant of it with the latter focusing on differences from the former. When processing that second record, the value for any property missing there should be adopted from item derived from first record rather than using its declared default. This is possible unless applying literal fallbacks here, only.
:::

#### Normative declaration of properties

Every item of a model consists of the properties listed here. Any property of originally collected record that isn't listed here won't make it into the resulting item. But as demonstrated before, terms deriving values for declared properties may access those additional properties of original record.

:::tip Case Sensitivity
Names of resulting properties are processed case-sensitively.

```yaml
properties:
  name: name
  NAME: NAME
```

This results in items each consisting of two properties `name` and `NAME`.

Terms are evaluated in context of original record and thus can access properties given there. This access works case-insensitively, though. That's why the example above is feeding both properties `name` and `NAME` with the same value found in a collected record. That record might contain property `name`, `NAME` or `NaMe` etc. A warning is displayed if the record happens to contain multiple elements using same case-insensitive name.

This case-insensitivity is by intention to limit the probability for errors e.g. by non-technical users manually maintaining the processed data.
:::

#### Collecting properties

On merging a property's values from different sources, more recently processed values are replacing its predecessors. 

By appending `[]` to a property's name here, the property becomes a _collecting property_, causing its values to be merged in a different way: all encountered values of a property are collected in a list.

```yaml
properties:
  name: name
  tags[]: tag
```

This example is combining a regular property with a collecting one. The value of property `tags` is always an array of values because of this declaration. Either element is originating from another record that has been processed as part of curing a database.

:::tip Don't get confused
A collecting property has per se nothing to do with [collections](#collections) to be described below.
:::

### collections

Another collection is generated for every entry in this map associating a custom [route](/about/routes.md) with a set of rules controlling generation of that collection.

```yaml
collections:
  "":
    properties:
      name: name
      role: role
  index/priority:
    key: priority
    properties:
      name: name
```

This example illustrates definition of two collections.

* A regular collection is defined to list all items of model with either item represented by its ID (implicitly included as `$id`), a name and a role.

* A second one is a keyed collection mapping from values of item property `priority` into lists of items with either priority value. For every such list, items are represented by their ID and a name.

See the [separate section on defining collections](/api/collections.md) for additional information.

### constraints

Every [model](../glossary.md#model)'s shape may optionally declare constraints to be considered and applied while [shaping model items](../about/curing.md#shaper) of resulting database.

#### singleSource

This boolean option declares whether multiple [records](../glossary.md#record) are accepted for describing a single [item](../glossary.md#item).

By default, properties found in multiple records are [compiled by merging all records and replacing or collecting values of properties found in different records](../about/curing.md#compiling-item-properties). However, this might be unintentional and cause data corruption if multiple records address the same item by accident. Declaring this constraint is preventing such cases for any item of the model.

### contributions

This part of a [model's shape](../about/shapes.md#model-shape) defines [contributions](../glossary.md#contribution) by items of that [model](../glossary.md#model) to [items](../glossary.md#item) of different models.

:::warning Contributions to same model
Technically, you can contribute to items of same model as well. It is expected to work as usual, though side effects are highly probable, thus you rather shouldn't do it. 
:::

The list of contributions is grouped by names of target models.

```yaml
models:
  sourceModel:
    contributions:
      targetModelA: ...
      targetModelB: ...
      targetModelC: ...
```

_These examples are partial definitions illustrating the structure, only._

For every target model, rules are listed to [compute values](../about/terms.md#contributing-to-items) based on the contributing item for properties of either target model to be adjusted by the contribution. 

```yaml
models:
  sourceModel:
    contributions:
      targetModelA:
        targetAProperty1: compute(from(sourcePropertyX))
        targetAProperty2: compute(from(sourcePropertyY))
```

This example defines, that every item of `sourceModel` is contributing to items of `targetModelA`. Either contribution is adjusting properties `targetAProperty1` and `targetAProperty2` of these items using values computed from properties `sourcePropertyX` and `sourcePropertyI` of that former item of `sourceModel`.

Adjustments support multiple modes

* replacement mode
* collection mode
* distribution mode

See below for [a detailed description of those three modes](#contributions-modes).

#### Picking target item

For every target model to receive contributions, a mandatory rule for computing the value of special property `$id` **must be** given to identify the item that's receiving all other values computed as part of a contribution.

```yaml
models:
  sourceModel:
    contributions:
      targetModelA:
        $id: deriveId(item)
```

The computed value can be a string or a number to select a single target item. In addition, the value can be an array of numbers and/or strings to select multiple items receiving a contribution from a single item of contributing model.

:::tip Contribute to missing items
Any selected item does not have to exist as it is implicitly created when picked. On creating items, [defaults](#defaults) of target model declared in its model shape are considered. 

In fact, contributions are often used with target models that aren't described by any record at all and thus never have any existing item without contributions.
:::

#### Contribution properties

Every other rule is required to compute a value to contribute per property of target model. The selected **property must be declared** in target model's shape for the sake of resulting database's integrity.

```yaml
models:
  book:
    properties:
      ...
    contributions:
      author:
        $id: author
        name: author    # this is rejected for `name` is no property of `author`
        refs: $id
  author:
    properties:
      refs: refs
    defaults:
      refs: array()
```

:::tip $id vs. $id
The mixed use of `$id` might confuse at first. If preceding the colon, it is used to identify the target item(s). Otherwise, it is exposing the contributing item's ID.

In example above

```yaml
$id: author
```

is declaring that the target item's ID is equivalent to the author mentioned in a contributing item of `book`, whereas

```yaml
refs: $id
```

is declaring that the contributing book's ID is used to update `refs` property of that identified `author` item
:::

#### Contributions modes

A contributing item is always contributing to properties of zero or more items of target model. The _contribution mode_ controls for every property how to adjust the related property of target item.

By default, contributed values per property are _replacing_ existing values of those properties in every selected target item.

:::tip Replacement mode
If multiple items contribute to the same item, only the last contributing item's value survives.
:::

```yaml
models:
  book:
    contributions:
      author:
        $id: author
        refs: $id
```

Every `book` contributes to items of `author` by putting its ID in either author's `refs` property, but only one book's ID is available at most, eventually.

The contribution mode can be adjusted per contribution rule, which is: per property of target model, by adding one or more suffixes to the property's name. Suffix `[]` is enabling _collection mode_ to be used instead of replacement mode.

:::tip Collection mode
If multiple items contribute to the same item, either item's contribution is kept resulting in an array of contributed values.
:::

```yaml
models:
  book:
    contributions:
      author:
        $id: author
        refs[]: $id
```

Every `book` contributes to items of `author` by putting its ID in either author's `refs` property. If existing value is an array, any contributed value is appended. Otherwise, any existing non-array value gets converted to a single-item array first. This way, all contributions survive.

Suffix `...` or `…` can be used to enable _distribution mode_ to be considered in addition to using either replacement mode or collection mode.

:::tip Distribution mode
On contributing to multiple items, every target item gets one of the values in a contributed array.
:::

```yaml
models:
  book:
    contributions:
      genre:
        $id: slugify( asArray( genre ) )
        label...: asArray( genre )
        refs[]: $id
```

In regard to collecting `book` IDs in `refs` property, this works similar to the previous example. In addition, it is putting name of genre as given per `book` into either item of `genre` that a book is contributing too.

:::tip Example
Given a book's item is listing genres "Crime" and "Comedy", it will contribute to two items of `genre`, one with ID `crime`, the other with ID `comedy`.

Because of the distribution mode enabled on `label`, the former item of genre gets label `"Crime"`, the other one `"Comedy"`. First selected item gets first value of array contributed for `label`, second selected item gets second value from that array.

Without distribution mode enabled, `label` will be `array( "Crime", "Comedy" )` for both items of `genre`.
:::

The suffixes for collection mode and distribution mode can be combined. However, order of definition is crucial: `[]` comes first, followed by `...` or `…`.

```yaml
models:
  book:
    contributions:
      genre:
        $id: slugify( asArray( genre ) )
        labels[]...: asArray( genre )
        refs[]: $id
```

In previous example of distribution mode, spelling per genre as found in last contributing book was used, only, because of replacement mode applying. This time, genres as found in either book are still distributed, but for every genre the different spellings are collected.

#### Optional contributions

Some item can contribute to _zero_ or more items of a target model. To help you debug processing issues due to bad data, a warning is emitted each time a contributing item fails to select items by proper ID(s).

```yaml
models:
  book:
    contributions:
      author:
        $id: author
        refs[]: $id
```

What if a compiled item of `book` does not come with a value in property `author`? This would result in contribution's `$id` being nullish, so that no item is selected for contribution. Since that is an issue most of the time, a warning is printed on the console.

However, if that's an expected possibility, contributions to a target model can be marked as _optional_ by appending a question mark to the target model's name in context of `contributions`.

```yaml
models:
  book:
    contributions:
      author?:
        $id: author
        refs[]: $id
```

This time, no warning about some item failing to select contribution target items is printed.

#### Example

Let's wrap this up a bit with an example:

```yaml
models:
  book:
	properties:
	  title: title
	  author: author
	  genre: asArray( genre )
    contributions:
      author:
        $id: slugify( asArray( author ) )
        titles: title
        books[]: $id
      genre:
        $id: slugify( asArray( genre ) )
        label...: asArray( genre )
        books[]: $id
  author:
    properties:
      titles: titles
      books: books
  genre:
    properties:
      label: label
      books: books
```

This is a brief example for a shape defining some contributions. 

* Contributions are declared in context of model `book`, thus its items are contributing here.

* Contributions of items of `book` regard models `author` and `genre`.

* For every item of `book`, the IDs of target items in `author` are computed by

  ```yaml
  $id: slugify( asArray( author ) )
  ```

  Thus, every author listed for a book is used to compute the ID of an item in model `author`.

* Every item of `book` contributes to properties `titles` and `books` per identified item of `author`. This means, a contribution is affecting those properties of selected target items, only.

* Contributions to property `titles` are [replacing](#contributions-modes): either author's record ends up with a single title of last processed book of that author.

  ```yaml
  titles: title
  ```

* Contributions to property `books` are collecting: either author's record ends up with an array of contributing books' IDs.

  ```yaml
  books[]: $id
  ```
  
* Accordingly, every item of `book` is declared to contribute to items of model `genre`. In addition, for every addressed genre its name is set in either target item.

* Last but not least, either target model of a contribution must be declared.


## Meta properties

[Property](#properties) names starting with `$` are reserved for _meta properties_. 

Meta properties are similar to regular properties of a model's [item](/glossary.md#item). However, they aren't visible most of the time. They aren't exposed in the resulting [database](/glossary.md#database), either. Instead, they are used for providing read access on certain data on evaluating [terms](/about/terms.md) and for internal management e.g. for keeping it related to its model or shape while passing [stages](/about/curing.md#stages) of [curing](/glossary.md#curing) the database. 

Some of these meta properties are created unconditionally by curing process. This means, you can't adjust them with rules of a shape. Some other meta properties exclusively result from processing rules of a shape. Some of these can be declared [per model](#model-shape), while others are used prior to detecting a record's model and thus work in [common set of rules](#common), only.

### $collection

This meta property is exposing part of a model shape defining collection currently created.

### $database

When computing terms as part of creating collections, additional meta property `$database` is exposing all collected items of either model in a two-level map:

```yaml
filter: price < $database.friend.john-doe.spentMoney / 10
```

This term used in a filter rule is testing if an item's `price` property is less than a tenth of `spentMoney` property of item with ID `john-doe` in model named `friend`.

:::tip
In versions prior to v0.2.0, this meta property was named `$models`. 
:::

### $id

Every collected record is meant to result in an item uniquely identifiable in context of its model. This meta property is extracting a record's resulting item ID right after discovering its model. You can declare it [per model](#models) or as part of the [common set of rules](#common).

```yaml
common:
  properties:
    $id: id

models:
  users:
    properties:
      $id: username
```

### $left

In terms used [on sorting items of a collection](../about/terms.md#on-sorting-regular-collections), this meta property is exposing left operator in a binary comparison operation.

### $model

This meta property is providing the name of the model a record is associated with. 

It is essential in the process of detecting a record's model in the resulting database. Thus, you cannot declare it per model but have to declare it as part of the [common set of rules](#common).

```yaml
common:
  properties:
    $model: model
```

### $original

On [compiling items from records](../about/terms.md#compiling-items) and on [contributing to items of other models](../about/terms.md#contributing-to-items) meta property `$original` is exposing raw data of the collected record.

In later processing stages, `$original` is still available, but due to the probable origin in multiple records, either item's `$original` is referring to merge of all these records' raw data.

### $post

When recompiling an item after [consuming contributions](../about/curing.md#processing-contributions) by other items, this boolean meta property is set so that terms can compute items accordingly.

### $right

In terms used [on sorting items of a collection](../about/terms.md#on-sorting-regular-collections), this meta property is exposing right operator in a binary comparison operation.

### $segments

Curing process is deriving this meta property from relative path name of a collected record's file by stripping off the file's extension and splitting the resulting string into segments of parent folders and result rest of file's name. 

:::tip Example
When searching folder **./data** for records it could find a file 

**./data/base/users/john-doe.1.record.yaml**

The file's relative path name is 

**base/users/john-doe.1.record.yaml**

By stripping off its extension it becomes 

**base/users/john-doe.1**

This is split into its segments **base**, **users** and **john-doe.1**, eventually. These are exposed as an array of strings named `$segments` and can be used in rules:

```yaml
common:
  properties:
    $model: item( $segments, length( $segments ) - 2 )
```
:::

### $shape

This meta property is associating every collected record with the [model shape](#model-shape) of its related model.


### $source

This meta property is exposed on computing terms as part of [sorting items of a collection](../about/terms.md#on-sorting-regular-collections).
